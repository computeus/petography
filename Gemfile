source 'https://rubygems.org'
ruby '2.3.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.7.1'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.3.13', '< 0.5'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
# gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in code to stop execution and get a debugger console
  gem 'byebug'

  gem 'rspec-rails', '~> 3.0'
  gem 'rspec-activemodel-mocks'
  gem 'rspec-collection_matchers'
  gem 'factory_girl_rails'
  gem 'capybara'
  gem 'database_cleaner'
  gem 'spring-commands-rspec'
  gem 'guard-rspec'
  gem 'shoulda-matchers'
  gem 'ffaker'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 3.1.1'

  # Spring speeds up development by keeping your application running in the
  # background. Read more: https://github.com/rails$
  gem 'spring'
  gem 'quiet_assets'
  gem 'annotate', require: false
  gem 'rails_best_practices', require: false
  gem 'rubocop', require: false
  gem 'rubocop-rspec'
  gem 'reek', require: false
  gem 'bullet'
  gem 'brakeman', require: false
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-rails', '~> 1.1', require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano3-unicorn', require: false
  gem 'capistrano-bundler', '~> 1.1.2', require: false
end

group :test do
  gem 'simplecov', require: false
end

group :assets do
  gem 'coffee-rails'
end

gem 'rails-i18n', '~> 4.0.0'
gem 'devise'
gem 'devise-i18n'
gem 'twitter-bootstrap-rails'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'mini_magick'
gem 'carrierwave'
gem 'nested_form_fields'
gem 'js-routes'
gem 'kaminari'
gem 'ice_cube'
gem 'schedulable'
gem 'public_activity'
gem 'ransack'
gem 'rollbar'
