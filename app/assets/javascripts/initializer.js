PETOGRAPHY_UTIL = {
  exec: function (controller, action) {
    var ns = PETOGRAPHY,
      action = ( action === undefined ) ? "init" : action;

    if (controller !== "" && ns[controller] && typeof ns[controller][action] == "function") {
      ns[controller][action]();
    }
  },

  init: function () {
    var body = document.body;
    var action = body.getAttribute("data-action");
    var controller = body.getAttribute("data-controller");

    PETOGRAPHY_UTIL.exec("common");
    PETOGRAPHY_UTIL.exec(controller);
    PETOGRAPHY_UTIL.exec(controller, action);
  }
};

$(document).on('turbolinks:load', PETOGRAPHY_UTIL.init);
