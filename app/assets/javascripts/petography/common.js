var PETOGRAPHY = window.PETOGRAPHY || {};

PETOGRAPHY.common = {
  init: function () {
    $('input.icheckbox').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });

    PETOGRAPHY.common.initialize_select2();
    PETOGRAPHY.common.bind_select2_initialization();
  }
}

PETOGRAPHY.common.initialize_select2 = function () {
  $('.select2').select2('destroy');
  $('.select2').select2({theme: 'bootstrap'});
}

PETOGRAPHY.common.bind_select2_initialization = function () {
  $('.add_nested_fields_link').click(function () {
    setTimeout(function () {
      PETOGRAPHY.common.initialize_select2();
    }, 1);
  });
}

PETOGRAPHY.common.get_pet_variants_on_pet_type_change = function (url, selector, target, include_blank) {
  selector.change(function () {
    $.get(url.replace('_pet_id_', $(this).val()), function (data) {
      target.empty();

      if(include_blank) {
        target.append($('<option>').text('').attr('value', ''));
      }

      $.each(data, function (i, obj) {
        target.append($('<option>').text(obj.name).attr('value', obj.id));
      });

      PETOGRAPHY.common.initialize_select2();
    });
  });
}

PETOGRAPHY.common.create_calendar = function (url) {
  $('#calendar').fullCalendar({
    events: url,
    header: {
      left: 'prev,next,today',
      center: 'title',
      right: 'listMonth,month'
    },
    views: {
      listMonth: {buttonText: 'Liste'},
      month: {buttonText: 'Aylık'}
    },
    defaultView: 'listMonth',
    eventLimit: true
  });
}

$(document).on('turbolinks:before-cache', function () {
  $('#calendar').fullCalendar('destroy');
});
