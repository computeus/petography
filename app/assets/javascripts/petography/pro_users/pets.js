// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var PETOGRAPHY = window.PETOGRAPHY || {};

PETOGRAPHY.petography_pro_users_pets = {
  show: function () {
    PETOGRAPHY.common.create_calendar(Routes.pro_users_pet_events_path(window.pet_id, {format: 'json'}));
  },
  new: function () {
    PETOGRAPHY.petography_pro_users_pets.get_pet_variants_on_pet_type_change();
  },
  create: function () {
    PETOGRAPHY.petography_pro_users_pets.get_pet_variants_on_pet_type_change();
  },
  edit: function () {
    PETOGRAPHY.petography_pro_users_pets.get_pet_variants_on_pet_type_change();
  },
  update: function () {
    PETOGRAPHY.petography_pro_users_pets.get_pet_variants_on_pet_type_change();
  }
}

PETOGRAPHY.petography_pro_users_pets.get_pet_variants_on_pet_type_change = function() {
  PETOGRAPHY.common.get_pet_variants_on_pet_type_change(Routes.pro_users_pet_type_variants_path('_pet_id_', {format: 'json'}), $('#pet_type_id'), $('#petography_pet_pet_variant_id'), false);
}
