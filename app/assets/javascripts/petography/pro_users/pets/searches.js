// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var PETOGRAPHY = window.PETOGRAPHY || {};

PETOGRAPHY.petography_pro_users_pets_searches = {
  index: function () {
    PETOGRAPHY.common.get_pet_variants_on_pet_type_change(
      Routes.pro_users_pet_type_variants_path('_pet_id_', {format: 'json'}),
      $('#q_variant_pet_type_id_eq'),
      $('#q_pet_variant_id_eq'),
      true
    );
  }
}
