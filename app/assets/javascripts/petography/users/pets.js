// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var PETOGRAPHY = window.PETOGRAPHY || {};

PETOGRAPHY.petography_users_pets = {
  show: function () {
    PETOGRAPHY.common.create_calendar(Routes.users_pet_events_path(window.pet_id, {format: 'json'}));
  },
  new: function () {
    PETOGRAPHY.petography_users_pets.get_pet_variants_on_pet_type_change();
  },
  create: function () {
    PETOGRAPHY.petography_users_pets.get_pet_variants_on_pet_type_change();
  },
  edit: function () {
    PETOGRAPHY.petography_users_pets.get_pet_variants_on_pet_type_change();
  },
  update: function () {
    PETOGRAPHY.petography_users_pets.get_pet_variants_on_pet_type_change();
  }
}

PETOGRAPHY.petography_users_pets.get_pet_variants_on_pet_type_change = function () {
  PETOGRAPHY.common.get_pet_variants_on_pet_type_change(Routes.users_pet_type_variants_path($('#pet_type_id').val(), {format: 'json'}), $('#pet_type_id'), false);
}
