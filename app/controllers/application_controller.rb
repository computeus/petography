# Rails default application controller.
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include PublicActivity::StoreController

  private

  def after_sign_in_path_for(resource)
    redirect_path_for_resource(resource)
  end

  def redirect_path_for_resource(resource)
    case
      when resource.pro_user?
        pro_users_pets_path
      when resource.user?
        users_pets_path
      else
        root_path
    end
  end
end
