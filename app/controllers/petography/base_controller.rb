module Petography
  class BaseController < ApplicationController
    before_action :authenticate_user!
    before_action :set_extra_field_local_variables, only: [:new, :edit, :create, :update]
    before_action :redirect_improper_users
    before_action :set_pets

    layout 'petography/users/application'

    private

    def set_extra_field_local_variables
      @extra_field_local_variables ||= {}
    end

    def set_pets
      @menu_pets = current_user.pets.includes(variant: :pet_type).limit(3)
    end
  end
end
