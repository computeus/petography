module Petography
  module ProUsers
    class BaseController < Petography::BaseController
      layout 'petography/pro_users/application'

      private

      def redirect_improper_users
        if !current_user.pro_user?
          redirect_to redirect_path_for_resource(current_user), notice: 'You are not accepted.'
        end
      end
    end
  end
end
