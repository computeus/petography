module Petography
  module ProUsers
    module PetTypes
      class VariantsController < Petography::ProUsers::BaseController
        def index
          pet_type = Petography::PetType.find(params[:pet_type_id])

          @variants = pet_type.variants
        end
      end
    end
  end
end
