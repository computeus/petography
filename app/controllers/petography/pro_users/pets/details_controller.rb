module Petography
  module ProUsers
    module Pets
      class DetailsController < Petography::ProUsers::BaseController
        def index
          @pet = Petography::Pet.find(params[:pet_id])
        end
      end
    end
  end
end
