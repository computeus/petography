module Petography
  module ProUsers
    module Pets
      class EventsController < Petography::ProUsers::BaseController
        def index
          pet            = Petography::Pet.find(params[:pet_id])

          params[:start] = Date.today if params[:start].blank?
          params[:end]   = Date.today.next_month if params[:end].blank?

          @events = pet
                      .treatment_event_occurrences
                      .includes(schedulable: :treatment)
                      .where(date: params[:start].to_date..params[:end].to_date)
        end
      end
    end
  end
end
