module Petography
  module ProUsers
    module Pets
      class SearchesController < Petography::ProUsers::BaseController
        def index
          @q         = Petography::Pet.includes(variant: :pet_type).search(params[:q])
          @pets      = @q.result(distinct: true).page(params[:page])
          @pet_types = Petography::PetType.all
          if params[:q].present? && params[:q][:variant_pet_type_id_eq].present?
            @pet_type = Petography::PetType.find(params[:q][:variant_pet_type_id_eq])
          else
            @pet_type = @pet_types.first
          end
          @pet_variants = @pet_type.variants
        end
      end
    end
  end
end
