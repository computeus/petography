module Petography
  module Users
    class BaseController < Petography::BaseController
      layout 'petography/users/application'

      private

      def redirect_improper_users
        if !current_user.user?
          redirect_to redirect_path_for_resource(current_user), notice: 'You are not accepted.'
        end
      end
    end
  end
end
