module Petography
  module Users
    class CuresController < Petography::Users::BaseController
      before_action :set_pet
      before_action :set_cure, only: [:show, :edit, :update, :destroy]
      before_action :set_variables_for_form, only: [:edit, :new, :create, :update]
      before_action :set_new_cure, only: :create
      before_action :build_treatment_event, only: [:edit, :create, :update]

      # GET /petography/users/cures/1
      # GET /petography/users/cures/1.json
      def show
      end

      # GET /petography/users/cures/new
      def new
        @cure = @pet.cures.new
        build_treatment_event
      end

      # GET /petography/users/cures/1/edit
      def edit
      end

      # POST /petography/users/cures
      # POST /petography/users/cures.json
      def create
        respond_to do |format|
          if @cure.save
            format.html { redirect_to users_pet_path(@pet), notice: 'Cure was successfully created.' }
            format.json { render :show, status: :created, location: @cure }
          else
            format.html { render :new }
            format.json { render json: @cure.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /petography/users/cures/1
      # PATCH/PUT /petography/users/cures/1.json
      def update
        respond_to do |format|
          if @cure.update(cure_params)
            format.html { redirect_to users_pet_path(@pet), notice: 'Cure was successfully updated.' }
            format.json { render :show, status: :ok, location: @cure }
          else
            format.html { render :edit }
            format.json { render json: @cure.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /petography/users/cures/1
      # DELETE /petography/users/cures/1.json
      def destroy
        @cure.destroy
        respond_to do |format|
          format.html { redirect_to users_pet_path(@pet), notice: 'Cure was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      # .includes(treatment_medicines: :medicine)
      # Bullet says unnecessary
      def set_cure
        @cure = @pet.cures.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def cure_params
        params.require(:petography_cure).permit(
          :applied_at, :description, :location_id,
          treatment_medicines_attributes: [:amount, :amount_type, :medicine_id, :_destroy, :id],
          reports_attributes:             [:report_date, :file, :_destroy, :id],
          disease_treatments_attributes:  [:disease_id, :_destroy, :id],
          treatment_event_attributes:     [:id, { schedule_attributes: Schedulable::ScheduleSupport.param_names }]
        )
      end

      def set_pet
        @pet = current_user.pets.find(params[:pet_id])
      end

      def set_variables_for_form
        @medicines                   = Petography::Medicine.all
        @locations                   = Petography::Location.includes(district: :city).all
        @diseases                    = Petography::Disease.all
        @extra_field_local_variables = { diseases: @diseases }
      end

      def set_new_cure
        @cure = @pet.cures.new(cure_params)
      end

      def build_treatment_event
        @cure.build_treatment_event if @cure.treatment_event.blank?
      end
    end
  end
end
