module Petography
  module Users
    module PetTypes
      class VariantsController < Petography::Users::BaseController
        def index
          pet_type = Petography::PetType.find(params[:pet_type_id])

          @variants = pet_type.variants
        end
      end
    end
  end
end
