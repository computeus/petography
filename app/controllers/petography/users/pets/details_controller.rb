module Petography
  module Users
    module Pets
      class DetailsController < Petography::Users::BaseController
        def index
          @pet = current_user.pets.find(params[:pet_id])
        end
      end
    end
  end
end
