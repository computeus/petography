module Petography
  module Users
    class PetsController < Petography::Users::BaseController
      before_action :set_pet, only: [:show, :edit, :update, :destroy]
      before_action :set_form_variables, only: [:edit, :new, :update, :create]
      before_action :set_new_pet, only: [:create]

      # GET /petography/users/pets
      # GET /petography/users/pets.json
      def index
        @pets = current_user.pets.includes(variant: :pet_type).page(params[:page])
      end

      # GET /petography/users/pets/1
      # GET /petography/users/pets/1.json
      def show
        # Bullet says unused
        # includes(:medicines).
        @activities = @pet.pet_activities.includes(:recipient, :trackable).page(params[:page])
      end

      # GET /petography/users/pets/new
      def new
        @pet = current_user.pets.new
      end

      # GET /petography/users/pets/1/edit
      def edit
      end

      # POST /petography/users/pets
      # POST /petography/users/pets.json
      def create
        respond_to do |format|
          if @pet.save
            format.html { redirect_to users_pet_path(@pet), notice: 'Pet was successfully created.' }
            format.json { render :show, status: :created, location: @pet }
          else
            format.html { render :new }
            format.json { render json: @pet.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /petography/users/pets/1
      # PATCH/PUT /petography/users/pets/1.json
      def update
        respond_to do |format|
          if @pet.update(pet_params)
            format.html { redirect_to users_pet_path(@pet), notice: 'Pet was successfully updated.' }
            format.json { render :show, status: :ok, location: @pet }
          else
            format.html { render :edit }
            format.json { render json: @pet.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /petography/users/pets/1
      # DELETE /petography/users/pets/1.json
      def destroy
        @pet.destroy
        respond_to do |format|
          format.html { redirect_to users_pets_path, notice: 'Pet was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_pet
        @pet = current_user.pets.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def pet_params
        params
          .require(:petography_pet)
          .permit(
            :name, :photo, :birthdate, :age, :pet_variant_id, :weight, :id_number, :chip_number,
            former_pet_diseases_attributes: [:disease_id, :id, :_destroy],
            former_pet_medicines_attributes: [:medicine_id, :id, :_destroy]
          )
      end

      def set_form_variables
        @pet_types = Petography::PetType.all

        @pet_type = if params[:pet_type_id].present?
                      Petography::PetType.find(params[:pet_type_id])
                    elsif @pet.present?
                      @pet.pet_type
                    else
                      @pet_types.first
                    end

        @pet_variants = @pet_type.variants
        @diseases     = Petography::Disease.all
        @medicines    = Petography::Medicine.all
      end

      def set_new_pet
        @pet = current_user.pets.new(pet_params)
      end
    end
  end
end
