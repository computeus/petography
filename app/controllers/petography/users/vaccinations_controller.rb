module Petography
  module Users
    class VaccinationsController < Petography::Users::BaseController
      before_action :set_pet
      before_action :set_vaccination, only: [:show, :edit, :update, :destroy]
      before_action :set_variables_for_form, only: [:edit, :new, :create, :update]
      before_action :set_new_vaccination, only: :create
      before_action :build_treatment_event, only: [:edit, :create, :update]

      # GET /petography/users/vaccinations/1
      # GET /petography/users/vaccinations/1.json
      def show
      end

      # GET /petography/users/vaccinations/new
      def new
        @vaccination = @pet.vaccinations.new
        build_treatment_event
      end

      # GET /petography/users/vaccinations/1/edit
      def edit
      end

      # POST /petography/users/vaccinations
      # POST /petography/users/vaccinations.json
      def create
        @vaccination = @pet.vaccinations.new(vaccination_params)

        respond_to do |format|
          if @vaccination.save
            format.html { redirect_to users_pet_path(@pet), notice: 'Vaccination was successfully created.' }
            format.json { render :show, status: :created, location: @vaccination }
          else
            format.html { render :new }
            format.json { render json: @vaccination.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /petography/users/vaccinations/1
      # PATCH/PUT /petography/users/vaccinations/1.json
      def update
        respond_to do |format|
          if @vaccination.update(vaccination_params)
            format.html { redirect_to users_pet_path(@pet), notice: 'Vaccination was successfully updated.' }
            format.json { render :show, status: :ok, location: @vaccination }
          else
            format.html { render :edit }
            format.json { render json: @vaccination.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /petography/users/vaccinations/1
      # DELETE /petography/users/vaccinations/1.json
      def destroy
        @vaccination.destroy
        respond_to do |format|
          format.html { redirect_to users_pet_path(@pet), notice: 'Vaccination was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private

      def set_vaccination
        # includes(treatment_medicines: :medicine).
        # Removed as it is unused says bullet gem
        @vaccination = @pet.vaccinations.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def vaccination_params
        params.require(:petography_vaccination).permit(
          :applied_at, :description, :location_id,
          treatment_medicines_attributes: [:amount, :amount_type, :medicine_id, :_destroy, :id],
          treatment_event_attributes:     [:id, { schedule_attributes: Schedulable::ScheduleSupport.param_names }]
        )
      end

      def set_pet
        @pet = current_user.pets.find(params[:pet_id])
      end

      def set_variables_for_form
        @medicines = Petography::Medicine.all
        @locations = Petography::Location.includes(district: :city).all
      end

      def set_new_vaccination
        @vaccination = @pet.vaccinations.new(vaccination_params)
      end

      def build_treatment_event
        @vaccination.build_treatment_event if @vaccination.treatment_event.blank?
      end
    end
  end
end
