module Petography
  module Users
    class WeightsController < Petography::Users::BaseController
      before_action :set_pet
      before_action :set_weight, only: [:edit, :update, :destroy]

      # GET /petography/users/weights
      # GET /petography/users/weights.json
      def index
        @weights = @pet.pet_weights.newest_first
      end

      # GET /petography/users/weights/new
      def new
        @weight = @pet.pet_weights.new
      end

      # GET /petography/users/weights/1/edit
      def edit
      end

      # POST /petography/users/weights
      # POST /petography/users/weights.json
      def create
        @weight = @pet.pet_weights.new(weight_params)

        respond_to do |format|
          if @weight.save
            format.html { redirect_to users_pet_weights_path(@pet), notice: 'Weight was successfully created.' }
            format.json { render :show, status: :created, location: @weight }
          else
            format.html { render :new }
            format.json { render json: @weight.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /petography/users/weights/1
      # PATCH/PUT /petography/users/weights/1.json
      def update
        respond_to do |format|
          if @weight.update(weight_params)
            format.html { redirect_to users_pet_weights_path(@pet), notice: 'Weight was successfully updated.' }
            format.json { render :show, status: :ok, location: @weight }
          else
            format.html { render :edit }
            format.json { render json: @weight.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /petography/users/weights/1
      # DELETE /petography/users/weights/1.json
      def destroy
        @weight.destroy
        respond_to do |format|
          format.html { redirect_to users_pet_weights_path(@pet), notice: 'Weight was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_pet
        @pet = current_user.pets.find(params[:pet_id])
      end

      def set_weight
        @weight = @pet.pet_weights.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def weight_params
        params.require(:petography_pet_weight).permit(:weight, :logged_at)
      end
    end
  end
end
