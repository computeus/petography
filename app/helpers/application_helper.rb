module ApplicationHelper
  def asset_controller_name
    controller.controller_path.tr('/', '_')
  end

  def item_class_name(item)
    item.class.name.demodulize.downcase
  end

  def render_extra_fields_for_form(item, locals = {})
    extra_fields_partial_path = "petography/users/#{item_class_name(item).pluralize}/form"
    render_extra_fields(extra_fields_partial_path, locals)
  end

  def render_extra_fields_for_show(item, locals = {})
    extra_fields_partial_path = "petography/users/#{item_class_name(item).pluralize}/show"
    render_extra_fields(extra_fields_partial_path, locals)
  end

  def render_extra_fields(extra_fields_partial_path, locals = {})
    if lookup_context.exists?("extra_fields", extra_fields_partial_path, true)
      render "#{extra_fields_partial_path}/extra_fields", locals
    end
  end
end
