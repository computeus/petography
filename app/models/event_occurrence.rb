# == Schema Information
#
# Table name: event_occurrences
#
#  id               :integer          not null, primary key
#  schedulable_id   :integer
#  schedulable_type :string(255)
#  date             :datetime
#  created_at       :datetime
#  updated_at       :datetime
#

class EventOccurrence < ActiveRecord::Base
  belongs_to :schedulable, polymorphic: true

  default_scope { order(date: :ASC) }
  scope :remaining, -> { where('date >= ?', Time.now) }
  scope :previous, -> { where('date < ?', Time.now) }
end
