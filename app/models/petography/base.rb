module Petography
  class Base < ActiveRecord::Base
    include PublicActivity::Model
    tracked owner: Proc.new { |controller, model| controller.try(:current_user) },
            recipient: :pet,
            on: [:create, :update]

    has_many :activities, class_name: "PublicActivity::Activity", as: :trackable, dependent: :destroy

    self.abstract_class = true

    self.table_name_prefix = 'petography_'

    def pet
      nil
    end
  end
end
