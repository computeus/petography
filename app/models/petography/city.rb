# == Schema Information
#
# Table name: petography_cities
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Petography
  class City < Petography::Base
    public_activity_off

    has_many :districts, inverse_of: :city, dependent: :destroy
  end
end
