# == Schema Information
#
# Table name: petography_treatments
#
#  id                      :integer          not null, primary key
#  applied_at              :datetime
#  description             :text(65535)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  pet_id                  :integer
#  location_id             :integer
#  type                    :string(255)
#  treatment_reports_count :integer          default(0)
#

module Petography
  class Cure < Petography::Treatment
    has_many :medicines, -> { uniq }, through: :treatment_medicines
    has_many :reports, inverse_of: :cure, class_name: Petography::TreatmentReport, dependent: :destroy, foreign_key: :treatment_id
    has_many :disease_treatments, dependent: :destroy, inverse_of: :cure, foreign_key: :treatment_id
    has_many :diseases, through: :disease_treatments

    belongs_to :location, inverse_of: :cures
    belongs_to :pet, inverse_of: :cures, counter_cache: true

    accepts_nested_attributes_for :reports, allow_destroy: true
    accepts_nested_attributes_for :disease_treatments, allow_destroy: true

    def disease_names
      diseases.pluck(:name).to_sentence
    end
  end
end
