# == Schema Information
#
# Table name: petography_diseases
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Petography
  class Disease < Petography::Base
    has_many :disease_treatments, dependent: :destroy, inverse_of: :disease
    has_many :cures, through: :disease_treatments
    has_many :former_pet_diseases, dependent: :destroy, inverse_of: :disease

    validates :name, presence: true, length: { in: 1..255 }, uniqueness: :true
  end
end
