# == Schema Information
#
# Table name: petography_disease_treatments
#
#  id           :integer          not null, primary key
#  disease_id   :integer
#  treatment_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

module Petography
  class DiseaseTreatment < Petography::Base
    belongs_to :disease, inverse_of: :disease_treatments, class_name: 'Petography::Disease'
    belongs_to :cure, inverse_of: :disease_treatments, class_name: 'Petography::Cure', foreign_key: :treatment_id

    validates :disease, presence: true, uniqueness: { scope: :cure }
    validates :cure, presence: true

    delegate :pet, to: :cure
  end
end
