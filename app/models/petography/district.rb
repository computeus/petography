# == Schema Information
#
# Table name: petography_districts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  city_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Petography
  class District < Petography::Base
    public_activity_off

    has_many :locations, inverse_of: :district

    belongs_to :city, inverse_of: :districts

    delegate :name, to: :city, prefix: true, allow_nil: true
  end
end
