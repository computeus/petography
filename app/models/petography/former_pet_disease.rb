# == Schema Information
#
# Table name: petography_former_pet_diseases
#
#  id         :integer          not null, primary key
#  pet_id     :integer
#  disease_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Petography
  class FormerPetDisease < Petography::Base
    belongs_to :pet, inverse_of: :former_pet_diseases, counter_cache: true
    belongs_to :disease, inverse_of: :former_pet_diseases

    validates :pet, presence: true
    validates :disease, presence: true, uniqueness: { scope: :pet }
  end
end
