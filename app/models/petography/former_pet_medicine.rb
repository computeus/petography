# == Schema Information
#
# Table name: petography_former_pet_medicines
#
#  id          :integer          not null, primary key
#  pet_id      :integer
#  medicine_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

module Petography
  class FormerPetMedicine < Petography::Base
    belongs_to :pet, inverse_of: :former_pet_medicines, counter_cache: true
    belongs_to :medicine, inverse_of: :former_pet_medicines

    validates :pet, presence: true
    validates :medicine, presence: true, uniqueness: { scope: :pet }
  end
end
