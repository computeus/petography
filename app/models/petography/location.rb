# == Schema Information
#
# Table name: petography_locations
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text(65535)
#  latitude    :decimal(9, 6)    default(0.0)
#  longitude   :decimal(9, 6)    default(0.0)
#  type        :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  district_id :integer
#  address     :string(255)
#  photo       :string(255)
#

module Petography
  class Location < Petography::Base
    mount_uploader :photo, Petography::LocationPhotoUploader

    has_many :cures, inverse_of: :location
    has_many :vaccinations, inverse_of: :location

    belongs_to :district, inverse_of: :locations

    validates :name, presence: true, length: { in: 1..255 }, uniqueness: { scope: :district }
    validates :description, presence: true, length: { in: 1..1000 }
    validates :latitude, presence: true
    validates :longitude, presence: true
    validates :address, presence: true
    validates :district, presence: true

    delegate :name, to: :district, prefix: true, allow_nil: true
    delegate :city_name, to: :district, allow_nil: true

    def label
      "#{name} - #{district_name} - #{city_name}"
    end
  end
end
