# == Schema Information
#
# Table name: petography_medicines
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text(65535)
#  photo       :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

module Petography
  class Medicine < Petography::Base
    has_many :treatment_medicines, inverse_of: :medicine, dependent: :destroy
    has_many :treatments, through: :treatment_medicines
    has_many :former_pet_medicines, inverse_of: :medicine, dependent: :destroy

    validates :name, presence: true, length: { in: 1..255 }
    validates :description, presence: true, length: { in: 10..1000 }
  end
end
