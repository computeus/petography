# == Schema Information
#
# Table name: petography_pets
#
#  id                         :integer          not null, primary key
#  name                       :string(255)
#  photo                      :string(255)
#  birthdate                  :date
#  pet_variant_id             :integer
#  user_id                    :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  chip_number                :string(255)
#  id_number                  :string(255)
#  cures_count                :integer          default(0)
#  vaccinations_count         :integer          default(0)
#  treatments_count           :integer          default(0)
#  former_pet_diseases_count  :integer          default(0)
#  former_pet_medicines_count :integer          default(0)
#

module Petography
  class Pet < Petography::Base
    ACTIVITY_TRACKABLE_TYPES = ['Petography::FormerPetDisease', 'Petography::FormerPetMedicine', 'Petography::Treatment', 'Petography::PetWeight']
    mount_uploader :photo, Petography::PetPhotoUploader

    has_many :treatments, inverse_of: :pet, dependent: :destroy
    has_many :treatment_events, through: :treatments
    has_many :treatment_event_occurrences, through: :treatment_events, source: :event_occurrences
    has_many :cures, inverse_of: :pet, dependent: :destroy
    has_many :cure_events, through: :cures, source: :treatment_event
    has_many :cure_event_occurrences, through: :cure_events, source: :event_occurrences
    has_many :vaccinations, inverse_of: :pet, dependent: :destroy
    has_many :vaccination_events, through: :vaccinations, source: :treatment_event
    has_many :vaccination_event_occurrences, through: :vaccination_events, source: :event_occurrences
    has_many :medicines, -> { uniq }, through: :cures
    has_many :cure_reports, through: :cures, source: :reports
    has_many :diseases, -> { uniq }, through: :cures
    has_many :pet_weights, inverse_of: :pet, dependent: :destroy
    has_many :former_pet_diseases, inverse_of: :pet, dependent: :destroy
    has_many :former_diseases, -> { uniq }, through: :former_pet_diseases, source: :disease
    has_many :former_pet_medicines, inverse_of: :pet, dependent: :destroy
    has_many :former_medicines, -> { uniq }, through: :former_pet_medicines, source: :medicine
    has_many :pet_activities,
             -> { where(
               recipient_type: 'Petography::Pet',
               trackable_type: ACTIVITY_TRACKABLE_TYPES
             ).order(created_at: :DESC) },
             class_name:  'PublicActivity::Activity',
             foreign_key: :recipient_id,
             dependent: :destroy

    belongs_to :variant, class_name: 'Petography::PetVariant', inverse_of: :pets, foreign_key: :pet_variant_id
    belongs_to :user, inverse_of: :pets

    validates :name, presence: true, length: { in: 2..255 }
    validates :birthdate, presence: true
    validates :variant, presence: true
    validates :user, presence: true
    validates :chip_number, length: { maximum: 255 }, allow_blank: true
    validates :id_number, length: { maximum: 255 }, allow_blank: true

    accepts_nested_attributes_for :former_pet_diseases, allow_destroy: true
    accepts_nested_attributes_for :former_pet_medicines, allow_destroy: true

    attr_writer :age, :weight

    before_validation :set_birthdate
    after_save :create_weight, :destroy_non_unique_former_objects

    delegate :pet_type, to: :variant, allow_nil: true
    delegate :label, to: :variant, prefix: true, allow_nil: true
    delegate :description, to: :next_treatment, prefix: true, allow_nil: true

    def age
      if birthdate.present?
        ((Time.zone.today - birthdate) / 365).to_f.round(1)
      else
        @age
      end
    end

    def label
      "#{name} - #{variant_label}"
    end

    def weight
      if pet_weights.latest.present?
        pet_weights.latest.weight
      else
        0
      end
    end

    def former_diseases_names
      former_relations_names(former_diseases)
    end

    def former_medicines_names
      former_relations_names(former_medicines)
    end

    def next_treatment
      treatment_event_occurrences.includes(:schedulable).first.schedulable.treatment if treatment_event_occurrences.present?
    end

    def next_treatment_date
      treatment_event_occurrences.first.date.to_date if treatment_event_occurrences.present?
    end

    def has_next_treatment?
      !!next_treatment
    end

    private

    def former_relations_names(relation)
      relation.collect(&:name).to_sentence
    end

    def set_birthdate
      self.birthdate = Time.zone.today - (@age.to_f * 365).to_i.days if @age.present? && birthdate.blank?
    end

    def create_weight
      pet_weights.create!(weight: @weight) if !@weight.blank?
    end

    def destroy_non_unique_former_objects
      relations = { former_pet_diseases: :disease_id, former_pet_medicines: :medicine_id }

      relations.each do |relation, field|
        ids = []

        elements = self.send(relation).all.to_a

        unless elements.size == elements.collect(&field).uniq.count
          elements.each do |elem|
            if ids.include?(elem.send(field))
              elem.destroy
            else
              ids << elem.send(field)
            end
          end
        end
      end
    end
  end
end
