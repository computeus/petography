# == Schema Information
#
# Table name: petography_pet_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Petography
  class PetType < Petography::Base
    has_many :variants, class_name: 'Petography::PetVariant', inverse_of: :pet_type

    validates :name, presence: true, length: { in: 2..255 }
    validates :code, presence: true, length: { in: 2..255 }
  end
end
