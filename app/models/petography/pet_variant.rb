# == Schema Information
#
# Table name: petography_pet_variants
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  code        :string(255)
#  pet_type_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

module Petography
  class PetVariant < Petography::Base
    has_many :pets, inverse_of: :variant

    belongs_to :pet_type, inverse_of: :variants

    validates :name, presence: true, length: { in: 2..255 }
    validates :code, presence: true, length: { in: 2..255 }

    delegate :name, to: :pet_type, prefix: true, allow_nil: true

    def label
      "#{name} - #{pet_type_name}"
    end
  end
end
