# == Schema Information
#
# Table name: petography_pet_weights
#
#  id         :integer          not null, primary key
#  pet_id     :integer
#  logged_at  :datetime
#  weight     :decimal(6, 2)    default(0.0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Petography
  class PetWeight < Petography::Base
    belongs_to :pet, inverse_of: :pet_weights

    validates :pet, presence: true
    validates :logged_at, presence: true
    validates :weight, inclusion: 0..9999, allow_blank: true

    after_initialize :set_logged_at

    scope :newest_first, -> { order(logged_at: :desc) }
    scope :latest, -> { newest_first.first }

    private

    def set_logged_at
      self.logged_at = Time.zone.now if logged_at.blank?
    end
  end
end
