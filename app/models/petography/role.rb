# == Schema Information
#
# Table name: petography_roles
#
#  id         :integer          not null, primary key
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Petography
  class Role < Petography::Base
    has_many :user_roles, inverse_of: :role, dependent: :destroy
    has_many :users, through: :user_roles

    validates :code, presence: true
  end
end
