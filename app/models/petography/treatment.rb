# == Schema Information
#
# Table name: petography_treatments
#
#  id                      :integer          not null, primary key
#  applied_at              :datetime
#  description             :text(65535)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  pet_id                  :integer
#  location_id             :integer
#  type                    :string(255)
#  treatment_reports_count :integer          default(0)
#

module Petography
  class Treatment < Petography::Base
    has_many :treatment_medicines, dependent: :destroy, inverse_of: :treatment

    has_one :treatment_event, dependent: :destroy, inverse_of: :treatment

    belongs_to :pet, inverse_of: :treatments, counter_cache: true

    validates :applied_at, presence: true
    validates :description, length: { in: 0..1000 }
    validates :pet, presence: true
    validates :location, presence: true

    after_initialize :set_applied_at
    before_validation :set_applied_at

    accepts_nested_attributes_for :treatment_medicines, allow_destroy: true
    accepts_nested_attributes_for :treatment_event, allow_destroy: true

    scope :newest_first, -> { order(applied_at: :desc) }
    scope :oldest_first, -> { order(applied_at: :asc) }

    def medicine_names
      medicines.pluck(:name).to_sentence
    end

    private

    def set_applied_at
      self.applied_at = Time.zone.now if applied_at.blank?
    end
  end
end
