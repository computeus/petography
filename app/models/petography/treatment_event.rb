# == Schema Information
#
# Table name: petography_treatment_events
#
#  id           :integer          not null, primary key
#  treatment_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

module Petography
  class TreatmentEvent < Petography::Base
    acts_as_schedulable :schedule, occurrences: :event_occurrences

    belongs_to :treatment, inverse_of: :treatment_event

    validates :treatment, presence: true

    delegate :pet, to: :treatment
  end
end
