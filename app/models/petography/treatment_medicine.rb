# == Schema Information
#
# Table name: petography_treatment_medicines
#
#  id           :integer          not null, primary key
#  treatment_id :integer
#  medicine_id  :integer
#  amount       :integer
#  amount_type  :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

module Petography
  class TreatmentMedicine < Petography::Base
    AMOUNT_TYPES = %w(ml cl dl lt mg g).freeze

    belongs_to :medicine, inverse_of: :treatment_medicines
    belongs_to :treatment, inverse_of: :treatment_medicines

    validates :medicine, presence: true
    validates :treatment, presence: true
    validates :amount, presence: true, inclusion: 0..99
    validates :amount_type, presence: true, inclusion: AMOUNT_TYPES

    delegate :pet, to: :treatment
  end
end
