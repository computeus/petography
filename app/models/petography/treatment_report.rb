# == Schema Information
#
# Table name: petography_treatment_reports
#
#  id           :integer          not null, primary key
#  report_date  :datetime
#  file         :string(255)
#  treatment_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

module Petography
  class TreatmentReport < Petography::Base
    mount_uploader :file, Petography::TreatmentReportFileUploader

    belongs_to :cure, inverse_of: :reports, foreign_key: :treatment_id, counter_cache: true

    validates :file, presence: true

    delegate :pet, to: :cure
  end
end
