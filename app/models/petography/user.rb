# == Schema Information
#
# Table name: petography_users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  fullname               :string(255)
#  photo                  :string(255)
#

module Petography
  class User < Petography::Base
    public_activity_off
    # Include default devise modules. Others available are:
    # :confirmable, :lockable
    devise :database_authenticatable, :registerable, :recoverable, :rememberable,
           :trackable, :validatable, :timeoutable, :omniauthable

    mount_uploader :photo, Petography::UserPhotoUploader

    tracked(owner: Proc.new { |controller, model| controller.try(:current_user) },
            on:    {
              update: -> model, c {
                !model.changes.except(
                  'sign_in_count', 'current_sign_in_at', 'last_sign_in_at', 'current_sign_in_ip', 'last_sign_in_ip'
                ).empty? }
            }
    )


    has_many :pets, inverse_of: :user
    has_many :user_roles, inverse_of: :user, dependent: :destroy
    has_many :roles, through: :user_roles

    def self.has_user_role
      Petography::Role.find_by(code: :user).users
    end

    def user?
      roles.where(code: :user).count > 0
    end

    def pro_user?
      roles.where(code: :pro_user).count > 0
    end
  end
end
