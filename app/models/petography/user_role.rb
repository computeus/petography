# == Schema Information
#
# Table name: petography_user_roles
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  role_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Petography
  class UserRole < Petography::Base
    belongs_to :user, inverse_of: :user_roles
    belongs_to :role, inverse_of: :user_roles

    validates :user, presence: true
    validates :role, presence: true
  end
end
