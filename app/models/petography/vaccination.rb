# == Schema Information
#
# Table name: petography_treatments
#
#  id                      :integer          not null, primary key
#  applied_at              :datetime
#  description             :text(65535)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  pet_id                  :integer
#  location_id             :integer
#  type                    :string(255)
#  treatment_reports_count :integer          default(0)
#

module Petography
  class Vaccination < Petography::Treatment
    has_many :medicines, -> { uniq }, through: :treatment_medicines

    belongs_to :location, inverse_of: :vaccinations
    belongs_to :pet, inverse_of: :vaccinations, counter_cache: true
  end
end
