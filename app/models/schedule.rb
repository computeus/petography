# == Schema Information
#
# Table name: schedules
#
#  id               :integer          not null, primary key
#  schedulable_id   :integer
#  schedulable_type :string(255)
#  date             :date
#  time             :time
#  rule             :string(255)
#  interval         :string(255)
#  day              :text(65535)
#  day_of_week      :text(65535)
#  until            :datetime
#  count            :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class Schedule < Schedulable::Model::Schedule
end
