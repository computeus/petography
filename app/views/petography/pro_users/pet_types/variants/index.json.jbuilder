json.array!(@variants) do |variant|
  json.extract! variant, :id, :name
end
