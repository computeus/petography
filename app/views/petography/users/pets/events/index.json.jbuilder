json.array!(@events) do |event|
  json.title event.schedulable.treatment.description
  json.start event.date.to_date
end
