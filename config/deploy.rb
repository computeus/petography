# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'petography'
set :repo_url, 'git@bitbucket.org:computeus/petography.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/deploy/petography'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :airbrussh.
set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, 'config/database.yml', 'config/secrets.yml'
append :linked_files, 'config/database.yml', 'config/unicorn.rb', 'config/secrets.yml'

# Default value for linked_dirs is []
# append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system'
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/sitemaps', 'public/uploads'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 3

set :rvm_ruby_version, '2.3.1'
set :rvm_type, :user

set :bundle_jobs, 2

set :unicorn_config_path, 'config/unicorn.rb'
set :unicorn_restart_sleep_time, 30

set :rollbar_token, Proc.new { YAML.load_file('config/secrets.yml')['default']['rollbar_key'] }
set :rollbar_env, Proc.new { fetch :stage }
set :rollbar_role, Proc.new { :app }

namespace :deploy do
  desc 'upload necessary files that are not in git'
  task :upload_files do
    on roles(:app) do |host|
      upload!('config/secrets.yml', "#{shared_path}/config/secrets.yml")
      upload!('config/database.yml', "#{shared_path}/config/database.yml")
      upload!('config/unicorn.rb', "#{shared_path}/config/unicorn.rb")
    end
  end
  after 'deploy:check:make_linked_dirs', :upload_files

  desc 'restart unicorn'
  task :restart do
    invoke 'unicorn:stop'
    sleep 10
    invoke 'unicorn:start'
  end
  after 'deploy:publishing', :restart
end
