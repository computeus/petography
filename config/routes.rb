Rails.application.routes.draw do
  scope module: :petography do
    namespace :users do
      get '/', to: 'home#index'

      resources :pets do
        resources :cures, except: :index
        resources :vaccinations, except: :index
        resources :weights, except: :show
        resources :details, only: :index, module: :pets
        resources :events, only: :index, module: :pets
      end

      resources :pet_types do
        resources :variants, only: :index, module: :pet_types
      end
    end

    namespace :pro_users do
      get '/', to: 'home#index'

      namespace :pets do
        get 'search', to: 'searches#index'
      end

      resources :pets do
        resources :cures, except: :index
        resources :vaccinations, except: :index
        resources :weights, except: :show
        resources :details, only: :index, module: :pets
        resources :events, only: :index, module: :pets
      end

      resources :pet_types do
        resources :variants, only: :index, module: :pet_types
      end
    end
  end

  devise_for :users, class_name: 'Petography::User'

  root 'pages#home'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
