class AddFullnameAndPhotoToPetographyUser < ActiveRecord::Migration
  def change
    add_column :petography_users, :fullname, :string
    add_column :petography_users, :photo, :string
  end
end
