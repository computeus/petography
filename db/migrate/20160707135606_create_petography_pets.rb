class CreatePetographyPets < ActiveRecord::Migration
  def change
    create_table :petography_pets do |t|
      t.string :name
      t.string :photo
      t.date :birthdate
      t.integer :pet_variant_id
      t.decimal :weight, precision: 6, scale: 2, default: 0
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :petography_pets, :name
    add_index :petography_pets, :birthdate
    add_index :petography_pets, :pet_variant_id
    add_index :petography_pets, :user_id
  end
end
