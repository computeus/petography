class CreatePetographyPetTypes < ActiveRecord::Migration
  def change
    create_table :petography_pet_types do |t|
      t.string :name
      t.string :code

      t.timestamps null: false
    end
    add_index :petography_pet_types, :name
    add_index :petography_pet_types, :code
  end
end
