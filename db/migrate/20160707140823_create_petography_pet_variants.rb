class CreatePetographyPetVariants < ActiveRecord::Migration
  def change
    create_table :petography_pet_variants do |t|
      t.string :name
      t.string :code
      t.integer :pet_type_id

      t.timestamps null: false
    end
    add_index :petography_pet_variants, :name
    add_index :petography_pet_variants, :code
    add_index :petography_pet_variants, :pet_type_id
  end
end
