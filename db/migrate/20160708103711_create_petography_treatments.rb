class CreatePetographyTreatments < ActiveRecord::Migration
  def change
    create_table :petography_treatments do |t|
      t.datetime :applied_at
      t.string :description

      t.timestamps null: false
    end
    add_index :petography_treatments, :applied_at
  end
end
