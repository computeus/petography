class CreatePetographyTreatmentMedicines < ActiveRecord::Migration
  def change
    create_table :petography_treatment_medicines do |t|
      t.integer :petography_treatment_id
      t.integer :petography_medicine_id
      t.integer :amount
      t.string :amount_type

      t.timestamps null: false
    end
    add_index :petography_treatment_medicines, :petography_treatment_id
    add_index :petography_treatment_medicines, :petography_medicine_id
    add_index :petography_treatment_medicines, :amount
    add_index :petography_treatment_medicines, :amount_type
  end
end
