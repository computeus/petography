class AddPetIdToTreatment < ActiveRecord::Migration
  def change
    add_column :petography_treatments, :pet_id, :integer
    add_index :petography_treatments, :pet_id
  end
end
