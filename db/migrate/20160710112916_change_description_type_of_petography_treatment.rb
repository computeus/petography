class ChangeDescriptionTypeOfPetographyTreatment < ActiveRecord::Migration
  def up
    change_column :petography_treatments, :description, :text
  end

  def down
    change_column :petography_treatments, :description, :string
  end
end