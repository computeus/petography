class CreatePetographyLocations < ActiveRecord::Migration
  def change
    create_table :petography_locations do |t|
      t.string :name
      t.text :description
      t.decimal :latitude, precision: 9, scale: 6, default: 0
      t.decimal :longitude, precision: 9, scale: 6, default: 0
      t.string :type

      t.timestamps null: false
    end

    add_index :petography_locations, :name
    add_index :petography_locations, :latitude
    add_index :petography_locations, :longitude
    add_index :petography_locations, :type
  end
end
