class AddLocationIdToPetographyTreatment < ActiveRecord::Migration
  def change
    add_column :petography_treatments, :location_id, :integer
    add_index :petography_treatments, :location_id
  end
end
