class CreatePetographyCities < ActiveRecord::Migration
  def change
    create_table :petography_cities do |t|
      t.string :name

      t.timestamps null: false
    end
    add_index :petography_cities, :name
  end
end
