class CreatePetographyDistricts < ActiveRecord::Migration
  def change
    create_table :petography_districts do |t|
      t.string :name
      t.integer :city_id

      t.timestamps null: false
    end
    add_index :petography_districts, :name
    add_index :petography_districts, :city_id
  end
end
