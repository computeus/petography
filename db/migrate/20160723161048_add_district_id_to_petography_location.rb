class AddDistrictIdToPetographyLocation < ActiveRecord::Migration
  def change
    add_column :petography_locations, :district_id, :integer
    add_index :petography_locations, :district_id
  end
end
