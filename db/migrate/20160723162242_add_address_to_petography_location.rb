class AddAddressToPetographyLocation < ActiveRecord::Migration
  def change
    add_column :petography_locations, :address, :string
  end
end
