class AddPhotoToPetographyLocation < ActiveRecord::Migration
  def change
    add_column :petography_locations, :photo, :string
  end
end
