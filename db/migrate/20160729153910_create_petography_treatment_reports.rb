class CreatePetographyTreatmentReports < ActiveRecord::Migration
  def change
    create_table :petography_treatment_reports do |t|
      t.datetime :report_date
      t.string :file
      t.integer :treatment_id

      t.timestamps null: false
    end
    add_index :petography_treatment_reports, :treatment_id
  end
end
