class CreatePetographyDiseases < ActiveRecord::Migration
  def change
    create_table :petography_diseases do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
