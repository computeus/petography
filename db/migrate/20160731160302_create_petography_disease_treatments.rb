class CreatePetographyDiseaseTreatments < ActiveRecord::Migration
  def change
    create_table :petography_disease_treatments do |t|
      t.integer :disease_id
      t.integer :treatment_id

      t.timestamps null: false
    end
    add_index :petography_disease_treatments, :disease_id
    add_index :petography_disease_treatments, :treatment_id
  end
end
