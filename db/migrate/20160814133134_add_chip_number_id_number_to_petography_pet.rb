class AddChipNumberIdNumberToPetographyPet < ActiveRecord::Migration
  def change
    add_column :petography_pets, :chip_number, :string
    add_index :petography_pets, :chip_number
    add_column :petography_pets, :id_number, :string
    add_index :petography_pets, :id_number
  end
end
