class CreatePetographyPetWeights < ActiveRecord::Migration
  def change
    create_table :petography_pet_weights do |t|
      t.integer :pet_id
      t.datetime :logged_at
      t.decimal :weight, precision: 6, scale: 2, default: 0

      t.timestamps null: false
    end
    add_index :petography_pet_weights, :pet_id
    add_index :petography_pet_weights, :logged_at
  end
end
