class RemoveWeightFromPetographyPet < ActiveRecord::Migration
  def change
    remove_column :petography_pets, :weight, :decimal, precision: 6, scale: 2, default: 0
  end
end
