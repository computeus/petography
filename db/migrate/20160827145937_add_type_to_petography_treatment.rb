class AddTypeToPetographyTreatment < ActiveRecord::Migration
  def change
    add_column :petography_treatments, :type, :string
    add_index :petography_treatments, :type
  end
end
