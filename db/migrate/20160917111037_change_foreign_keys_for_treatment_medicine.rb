class ChangeForeignKeysForTreatmentMedicine < ActiveRecord::Migration
  def change
    rename_column :petography_treatment_medicines, :petography_medicine_id, :medicine_id
    rename_column :petography_treatment_medicines, :petography_treatment_id, :treatment_id
  end
end
