class CreatePetographyFormerDiseases < ActiveRecord::Migration
  def change
    create_table :petography_former_pet_diseases do |t|
      t.integer :pet_id
      t.integer :disease_id

      t.timestamps null: false
    end
    add_index :petography_former_pet_diseases, :pet_id
    add_index :petography_former_pet_diseases, :disease_id
  end
end
