class CreatePetographyFormerPetMedicines < ActiveRecord::Migration
  def change
    create_table :petography_former_pet_medicines do |t|
      t.integer :pet_id
      t.integer :medicine_id

      t.timestamps null: false
    end
    add_index :petography_former_pet_medicines, :pet_id
    add_index :petography_former_pet_medicines, :medicine_id
  end
end
