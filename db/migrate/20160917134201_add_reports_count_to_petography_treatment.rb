class AddReportsCountToPetographyTreatment < ActiveRecord::Migration
  def change
    add_column :petography_treatments, :treatment_reports_count, :integer, default: 0
  end
end
