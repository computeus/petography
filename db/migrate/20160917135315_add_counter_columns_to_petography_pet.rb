class AddCounterColumnsToPetographyPet < ActiveRecord::Migration
  def change
    add_column :petography_pets, :cures_count, :integer, default: 0
    add_column :petography_pets, :vaccinations_count, :integer, default: 0
    add_column :petography_pets, :treatments_count, :integer, default: 0
  end
end
