class AddFormerPetMedicinesCountToPetographyPet < ActiveRecord::Migration
  def change
    add_column :petography_pets, :former_pet_medicines_count, :integer, default: 0
  end
end
