class CreatePetographyTreatmentEvents < ActiveRecord::Migration
  def change
    create_table :petography_treatment_events do |t|
      t.integer :treatment_id

      t.timestamps null: false
    end
    add_index :petography_treatment_events, :treatment_id
  end
end
