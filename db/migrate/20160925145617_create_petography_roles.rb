class CreatePetographyRoles < ActiveRecord::Migration
  def change
    create_table :petography_roles do |t|
      t.string :code

      t.timestamps null: false
    end
    add_index :petography_roles, :code
  end
end
