class CreatePetographyUserRoles < ActiveRecord::Migration
  def change
    create_table :petography_user_roles do |t|
      t.integer :user_id
      t.integer :role_id

      t.timestamps null: false
    end
    add_index :petography_user_roles, :user_id
    add_index :petography_user_roles, :role_id
  end
end
