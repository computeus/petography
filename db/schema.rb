# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160925145652) do

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id",   limit: 4
    t.string   "trackable_type", limit: 255
    t.integer  "owner_id",       limit: 4
    t.string   "owner_type",     limit: 255
    t.string   "key",            limit: 255
    t.text     "parameters",     limit: 65535
    t.integer  "recipient_id",   limit: 4
    t.string   "recipient_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "event_occurrences", force: :cascade do |t|
    t.integer  "schedulable_id",   limit: 4
    t.string   "schedulable_type", limit: 255
    t.datetime "date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "petography_cities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "petography_cities", ["name"], name: "index_petography_cities_on_name", using: :btree

  create_table "petography_disease_treatments", force: :cascade do |t|
    t.integer  "disease_id",   limit: 4
    t.integer  "treatment_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "petography_disease_treatments", ["disease_id"], name: "index_petography_disease_treatments_on_disease_id", using: :btree
  add_index "petography_disease_treatments", ["treatment_id"], name: "index_petography_disease_treatments_on_treatment_id", using: :btree

  create_table "petography_diseases", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "petography_districts", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "city_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "petography_districts", ["city_id"], name: "index_petography_districts_on_city_id", using: :btree
  add_index "petography_districts", ["name"], name: "index_petography_districts_on_name", using: :btree

  create_table "petography_former_pet_diseases", force: :cascade do |t|
    t.integer  "pet_id",     limit: 4
    t.integer  "disease_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "petography_former_pet_diseases", ["disease_id"], name: "index_petography_former_pet_diseases_on_disease_id", using: :btree
  add_index "petography_former_pet_diseases", ["pet_id"], name: "index_petography_former_pet_diseases_on_pet_id", using: :btree

  create_table "petography_former_pet_medicines", force: :cascade do |t|
    t.integer  "pet_id",      limit: 4
    t.integer  "medicine_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "petography_former_pet_medicines", ["medicine_id"], name: "index_petography_former_pet_medicines_on_medicine_id", using: :btree
  add_index "petography_former_pet_medicines", ["pet_id"], name: "index_petography_former_pet_medicines_on_pet_id", using: :btree

  create_table "petography_locations", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.decimal  "latitude",                  precision: 9, scale: 6, default: 0.0
    t.decimal  "longitude",                 precision: 9, scale: 6, default: 0.0
    t.string   "type",        limit: 255
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.integer  "district_id", limit: 4
    t.string   "address",     limit: 255
    t.string   "photo",       limit: 255
  end

  add_index "petography_locations", ["district_id"], name: "index_petography_locations_on_district_id", using: :btree
  add_index "petography_locations", ["latitude"], name: "index_petography_locations_on_latitude", using: :btree
  add_index "petography_locations", ["longitude"], name: "index_petography_locations_on_longitude", using: :btree
  add_index "petography_locations", ["name"], name: "index_petography_locations_on_name", using: :btree
  add_index "petography_locations", ["type"], name: "index_petography_locations_on_type", using: :btree

  create_table "petography_medicines", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "photo",       limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "petography_pet_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "code",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "petography_pet_types", ["code"], name: "index_petography_pet_types_on_code", using: :btree
  add_index "petography_pet_types", ["name"], name: "index_petography_pet_types_on_name", using: :btree

  create_table "petography_pet_variants", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "code",        limit: 255
    t.integer  "pet_type_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "petography_pet_variants", ["code"], name: "index_petography_pet_variants_on_code", using: :btree
  add_index "petography_pet_variants", ["name"], name: "index_petography_pet_variants_on_name", using: :btree
  add_index "petography_pet_variants", ["pet_type_id"], name: "index_petography_pet_variants_on_pet_type_id", using: :btree

  create_table "petography_pet_weights", force: :cascade do |t|
    t.integer  "pet_id",     limit: 4
    t.datetime "logged_at"
    t.decimal  "weight",               precision: 6, scale: 2, default: 0.0
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
  end

  add_index "petography_pet_weights", ["logged_at"], name: "index_petography_pet_weights_on_logged_at", using: :btree
  add_index "petography_pet_weights", ["pet_id"], name: "index_petography_pet_weights_on_pet_id", using: :btree

  create_table "petography_pets", force: :cascade do |t|
    t.string   "name",                       limit: 255
    t.string   "photo",                      limit: 255
    t.date     "birthdate"
    t.integer  "pet_variant_id",             limit: 4
    t.integer  "user_id",                    limit: 4
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "chip_number",                limit: 255
    t.string   "id_number",                  limit: 255
    t.integer  "cures_count",                limit: 4,   default: 0
    t.integer  "vaccinations_count",         limit: 4,   default: 0
    t.integer  "treatments_count",           limit: 4,   default: 0
    t.integer  "former_pet_diseases_count",  limit: 4,   default: 0
    t.integer  "former_pet_medicines_count", limit: 4,   default: 0
  end

  add_index "petography_pets", ["birthdate"], name: "index_petography_pets_on_birthdate", using: :btree
  add_index "petography_pets", ["chip_number"], name: "index_petography_pets_on_chip_number", using: :btree
  add_index "petography_pets", ["id_number"], name: "index_petography_pets_on_id_number", using: :btree
  add_index "petography_pets", ["name"], name: "index_petography_pets_on_name", using: :btree
  add_index "petography_pets", ["pet_variant_id"], name: "index_petography_pets_on_pet_variant_id", using: :btree
  add_index "petography_pets", ["user_id"], name: "index_petography_pets_on_user_id", using: :btree

  create_table "petography_roles", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "petography_roles", ["code"], name: "index_petography_roles_on_code", using: :btree

  create_table "petography_treatment_events", force: :cascade do |t|
    t.integer  "treatment_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "petography_treatment_events", ["treatment_id"], name: "index_petography_treatment_events_on_treatment_id", using: :btree

  create_table "petography_treatment_medicines", force: :cascade do |t|
    t.integer  "treatment_id", limit: 4
    t.integer  "medicine_id",  limit: 4
    t.integer  "amount",       limit: 4
    t.string   "amount_type",  limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "petography_treatment_medicines", ["amount"], name: "index_petography_treatment_medicines_on_amount", using: :btree
  add_index "petography_treatment_medicines", ["amount_type"], name: "index_petography_treatment_medicines_on_amount_type", using: :btree
  add_index "petography_treatment_medicines", ["medicine_id"], name: "index_petography_treatment_medicines_on_medicine_id", using: :btree
  add_index "petography_treatment_medicines", ["treatment_id"], name: "index_petography_treatment_medicines_on_treatment_id", using: :btree

  create_table "petography_treatment_reports", force: :cascade do |t|
    t.datetime "report_date"
    t.string   "file",         limit: 255
    t.integer  "treatment_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "petography_treatment_reports", ["treatment_id"], name: "index_petography_treatment_reports_on_treatment_id", using: :btree

  create_table "petography_treatments", force: :cascade do |t|
    t.datetime "applied_at"
    t.text     "description",             limit: 65535
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "pet_id",                  limit: 4
    t.integer  "location_id",             limit: 4
    t.string   "type",                    limit: 255
    t.integer  "treatment_reports_count", limit: 4,     default: 0
  end

  add_index "petography_treatments", ["applied_at"], name: "index_petography_treatments_on_applied_at", using: :btree
  add_index "petography_treatments", ["location_id"], name: "index_petography_treatments_on_location_id", using: :btree
  add_index "petography_treatments", ["pet_id"], name: "index_petography_treatments_on_pet_id", using: :btree
  add_index "petography_treatments", ["type"], name: "index_petography_treatments_on_type", using: :btree

  create_table "petography_user_roles", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "role_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "petography_user_roles", ["role_id"], name: "index_petography_user_roles_on_role_id", using: :btree
  add_index "petography_user_roles", ["user_id"], name: "index_petography_user_roles_on_user_id", using: :btree

  create_table "petography_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "fullname",               limit: 255
    t.string   "photo",                  limit: 255
  end

  add_index "petography_users", ["email"], name: "index_petography_users_on_email", unique: true, using: :btree
  add_index "petography_users", ["reset_password_token"], name: "index_petography_users_on_reset_password_token", unique: true, using: :btree

  create_table "schedules", force: :cascade do |t|
    t.integer  "schedulable_id",   limit: 4
    t.string   "schedulable_type", limit: 255
    t.date     "date"
    t.time     "time"
    t.string   "rule",             limit: 255
    t.string   "interval",         limit: 255
    t.text     "day",              limit: 65535
    t.text     "day_of_week",      limit: 65535
    t.datetime "until"
    t.integer  "count",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
