# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
load "#{Rails.root}/db/seeds/petography/pet_variants.rb"
load "#{Rails.root}/db/seeds/petography/districts.rb"

load "#{Rails.root}/db/seeds/petography/veterinaries.rb"
load "#{Rails.root}/db/seeds/petography/pet_shops.rb"
load "#{Rails.root}/db/seeds/petography/roles.rb"
load "#{Rails.root}/db/seeds/petography/users.rb"
load "#{Rails.root}/db/seeds/petography/medicines.rb"
load "#{Rails.root}/db/seeds/petography/diseases.rb"
load "#{Rails.root}/db/seeds/petography/pets.rb"
load "#{Rails.root}/db/seeds/petography/treatments.rb"
load "#{Rails.root}/db/seeds/petography/former_diseases.rb"
load "#{Rails.root}/db/seeds/petography/former_medicines.rb"
