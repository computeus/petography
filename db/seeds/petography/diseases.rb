puts 'Adding Diseases'

Petography::Disease.find_each(&:destroy)

Petography::Disease.create!(name: 'Kedi Nezlesi')
Petography::Disease.create!(name: 'İdrar Yolu Enfeksiyonu')
Petography::Disease.create!(name: 'Böbrek Yetmezliği')
Petography::Disease.create!(name: 'Karaciğer Yetmezliği')
Petography::Disease.create!(name: 'Diyabet')
Petography::Disease.create!(name: 'Uyuz')
