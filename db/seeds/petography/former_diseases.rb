puts 'Adding Former Diseases For Pets'

Petography::FormerPetDisease.find_each(&:destroy)

Petography::Pet.find_each do |pet|
  3.times do
    pet.former_pet_diseases.create(disease: Petography::Disease.all.sample)
  end
end
