puts 'Adding Former Medicines For Pets'

Petography::FormerPetMedicine.find_each(&:destroy)

Petography::Pet.find_each do |pet|
  3.times do
    pet.former_pet_medicines.create(medicine: Petography::Medicine.all.sample)
  end
end
