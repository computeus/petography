puts 'Adding Medicines'

Petography::Medicine.find_each(&:destroy)

Petography::Medicine.create!(
  name:        'Beaphar Pire Damlası',
  description: 'Beaphar Pire Damlası'
)
Petography::Medicine.create!(
  name:        'Zoetis VMP Tablet Kedi-Köpek Tüy Dökülme Önleyici Tablet',
  description: 'Zoetis VMP Tablet Kedi-Köpek Tüy Dökülme Önleyici Tablet'
)
Petography::Medicine.create!(
  name:        'Provet Eye Drop Göz Temizleme Damlası',
  description: 'Provet Eye Drop Göz Temizleme Damlası'
)
Petography::Medicine.create!(
  name:        'Nutradyl Köpek Eklem Destekleyici Tablet',
  description: 'Nutradyl Köpek Eklem Destekleyici Tablet'
)
Petography::Medicine.create!(
  name:        'Promega Omega 6 Vitamin Mineral Tüy Dökümüne En Etkili Çözüm',
  description: 'Promega Omega 6 Vitamin Mineral Tüy Dökümüne En Etkili Çözüm'
)
Petography::Medicine.create!(
  name:        'EPİSCENT Köpek cilt ve tüy bakım ürünü',
  description: 'EPİSCENT Köpek cilt ve tüy bakım ürünü'
)
Petography::Medicine.create!(
  name:        'Gimpet Cheese Paste Biyotinli Peynirli Macun',
  description: 'Gimpet Cheese Paste Biyotinli Peynirli Macun'
)
