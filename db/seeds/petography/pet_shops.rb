puts 'Adding Pet Shops'

Petography::PetShop.find_each(&:destroy)

Petography::PetShop.create!(
  name:        'Baran Petshop',
  description: 'Baran Petshop',
  latitude:    40.917943,
  longitude:   29.258809,
  district:    Petography::District.find_by(name: 'Pendik'),
  address:     'Ertuğrul Gazi, Aydos Cd., 34909'
)
Petography::PetShop.create!(
  name:        'Mercan Akvaryum',
  description: 'Mercan Akvaryum',
  latitude:    40.917167,
  longitude:   29.207310,
  district:    Petography::District.find_by(name: 'Kartal'),
  address:     'Atatürk Cad./yonca Sok. No:4, 34880'
)
Petography::PetShop.create!(
  name:        'Şadanlar Pet',
  description: 'Üretici',
  latitude:    40.899538,
  longitude:   29.185165,
  district:    Petography::District.find_by(name: 'Kartal'),
  address:     'Esentepe Mah. İnönü Cad. Yasa Kule No:6/1 Kat:12 D:49, 34870'
)
