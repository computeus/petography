load "#{Rails.root}/db/seeds/petography/pet_types.rb"

Petography::PetVariant.delete_all

print 'Adding Cat Variants'

cat = Petography::PetType.find_by(code: :cat)

counter = 0

cats = [
  'Abyssinian', 'American Bobtail', 'American Curl', 'American Keuda',
  'American Shorthair', 'American Whitehair', 'Ankara', 'Australian Mist',
  'Balinese', 'Bengal', 'Birman', 'Bombay', 'Brazilian Shorthair',
  'Biritish Shorthair', 'Burmese', 'Silver Burmese', 'California Spangled',
  'Chartreux', 'Chinchilla', 'Colorpoint Shorthair', 'Cornish Rex', 'Cymric',
  'Devon Rex', 'Egyptian Mau', 'European Burmese', 'European Shorthair',
  'Exotic Shorthair', 'Havana Brown', 'Himalayan', 'Honey Bear',
  'Japanese Bobtail', 'Javanese', 'Kashmir', 'Korat', 'Laperm', 'Maine Coon',
  'Manx', 'Mojeve Desert Cat', 'Munchkin', 'Nebulung', 'Norwegian Forest',
  'Ocicat', 'Oriental Longhair', 'Oriental Shorthair', 'Persian', 'Pixie Bob',
  'Ragamuffin', 'Ragdoll', 'Russian Blue', 'Savannah', 'Scottish Fold Longhair',
  'Scottish Fold Shorthair', 'Selkirk Rex', 'Siamese', 'Siberian', 'Singapura',
  'Snowshoe', 'Sokoke', 'Somali', 'Sphynx', 'Tiffanie', 'Tonkinese', 'Tekir',
  'Van', 'York Chocolate'
]

cats.each do |cat_name|
  cat.variants.create!(name: cat_name, code: cat_name.parameterize)
  print "\rAdding Cat Variants: #{counter += 1} / #{cats.count}"
end

puts ''

print 'Adding Dog Variants'

dog = Petography::PetType.find_by(code: :dog)

counter = 0

dogs = [
  'Affenpinscher', 'Afghan hound', 'Aidi', 'Airedale terrier',
  'Akbash dog', 'Akita Inu', 'Alapaha Blue Blood Bulldog',
  'Alaskan husky', 'Alaskan klee kai', 'Alaskan malamute',
  'German Shepherd Dog', 'German Shorthaired Pointer', 'German Pinscher',
  'German Wirehaired Pointer', 'Alpine Dachsbracke', 'Alsatian shepalute',
  'Old German Cattledog', 'Anatolian Shepherd Dog', 'American akita',
  'American bulldog', 'American cocker spaniel', 'American Eskimo dog',
  'American Foxhound', 'American hairless terrier', 'American pit bull terrier',
  'American Staffordshire Terrier', 'American Toy Terrier',
  'American Water Spaniel', 'Medium-sized Anglo-French Hound',
  'Appenzeller Sennenhund', 'Ariege Hound', 'Australian Cattle Dog',
  'Australian Kelpie', 'Australian Shepherd', 'Australian Silky Terrier',
  'Australian Stumpy Tail Cattle Dog', 'Australian terrier', 'Azawakh',
  'Bangkaew Dog', 'Barbet', 'Bardino', 'Barzoï', 'Basenji',
  'Norman Artesian Basset', 'Blue Gascony Basset', 'Fawn Brittany Basset',
  'Basset Hound', 'West Highland White Terrier', 'Bavarian Mountain Hound',
  'Beagle', 'Bearded Collie', 'Beauceron', 'Bedlington terrier',
  'Belgian Shepherd', 'Brussels Griffon', 'Bergamasco Sheppard',
  'White Swiss Shepherd', 'Picardy Shepherd', 'Pyrenean Sheepdog',
  'Bernese Mountain Dog', 'Bichon Frisé', 'Havanese', 'Black and Tan Coonhound',
  'Black Russian Terrier', 'Blue Picardy Spaniel', 'Bluetick Coonhound',
  'Boerboel', 'Bohemian Spotted Dog', 'Boxer', 'Bolognese', 'Bolonka franzuska',
  'Border Collie', 'Border Terrier', 'Bosnian Coarse-Haired Hound',
  'Boston Terrier', 'Bouvier des Flandres', 'Boykin Spaniel',
  'French Pointing Dog - Pyrenean type', 'Bracco Italiano',
  'Austrian Black and Tan Hound', 'Auvergne Pointing Dog',
  'Bourbonnais Pointing Dog', 'Braque Saint-Germain', 'Brittany Spaniel',
  'Briard', 'Medium Vendeen Griffon', 'Broholmer', 'Bulldog', 'Bull Terrier',
  'Bullmastiff', 'Majorca Mastiff', 'Majorca Shepherd Dog', 'Cairn Terrier',
  'Galician Palleiro Dog, Palleiro Shepherd Dog, Galician Cattledog, Palleiro',
  'Canaan Dog', 'Canadian Inuit Dog, Canadian Eskimo Dog',
  'Cane Corso, Italian Corso Dog', 'Maremma and Abruzzes Sheepdog', 'Poodle',
  'Azores Cattle Dog', 'Portuguese Sheepdog', 'Caravan Hound', 'Carolina Dog',
  'Catahoula Leopard Dog', 'Catahoula Bulldog',
  'Caucasian Ovcharka, Caucasian Mountain Dog', 'Cavalier King Charles Spaniel',
  'Central Asia Shepherd Dog', 'Bohemian wire-haired Pointing Griffon',
  'Cesky Terrier', 'Czechoslovakian Wolfdog',
  'Polish Greyhound, Polish Sighthound', 'Chesapeake Bay Retriever',
  'Pyrenean Mountain Dog', 'Belgian Shepherd Groenendael',
  'Belgian Shepherd Laekenois', 'Belgian Shepherd Malinois',
  'Belgian Shepherd Tervuren', 'Estrela Mountain Dog', 'Portugese Cattle Dog',
  'Bloodhound', 'Artois Hound', 'Chihuahua', 'Chinook', 'Bohemian Shepherd',
  'Chow Chow', 'Romanian Carpathian Shepherd Dog',
  'Romanian de Bucovina Shepherd Dog', 'Romanian Mioritic Shepherd Dog',
  "Cirneco dell'Etna", 'Clumber Spaniel', 'Cockapoo', 'Papillon',
  'Coton de Tulear', 'Cretan Hound', 'Cursinu', 'Curly Coated Retriever',
  'Dachshund', 'Dalmatian', 'Dandie Dinmont Terrier', 'Danish/Swedish Farm Dog',
  'Deerhound', 'German Hound', 'Great Dane', 'German Longhaired Pointer',
  'German Rough-haired Pointing Dog', 'German Spitz', 'German Spaniel',
  'Tibetan Mastiff', 'Doberman Pinscher', 'Argentine Dogo', 'Canary Dog',
  'Dogue de Bordeaux, French Mastiff', 'Dutch Partridge Dog', 'Drever',
  'Norwegian Hound', 'Elo', 'English Cocker Spaniel', 'English Coonhound',
  'English Foxhound', 'English Springer Spaniel', 'English Setter',
  'English Toy Terrier, King Charles Spaniel', 'Entelbuch Mountain Dog',
  'French Spaniel', 'Picardy Spaniel', 'Pont Audemer Spaniel',
  'Transylvanian Hound', 'Eurasier', 'Brazilian Mastiff',
  'Flat-Coated Retriever', 'Fox Terrier', 'French Bulldog',
  'Spanish Greyhound', 'Old Danish Pointer', 'Glen of Imaal Terrier',
  'Golden Retriever', 'Polish Hunting Dog, Polish Scenthound',
  'Gordon Setter', 'Catalan Sheepdog', 'Great Anglo-French Tricolour Hound',
  'Great Anglo-French White and Orange Hound',
  'Great Anglo-French White and Black Hound', 'Large Vendeen Griffon Basset',
  'Great Gascony Hound, Great Gascony Blue', 'Greenland Dog', 'Greyhound',
  'Fawn Brittany Griffon', 'Blue Gascony Griffon',
  'Wirehaired Pointing Griffon, Korthals Griffon', 'Griffon Nivernais',
  'Greater Swiss Mountain Dog', 'Halden Hound', 'Hamilton Hound',
  'Hanover Hound', 'Harrier', 'Hellenic Hound', 'Hokkaido inu',
  'Dutch Shepherd Dog', 'Dutch Smoushond', 'Hovawart', 'Croatian Sheepdog',
  'South Russian Ovcharka', 'Huntaway', 'Irish Red And White Setter',
  'Irish Setter', 'Soft-Coated Wheaten Terrier', 'Irish Terrier',
  'Irish Water Spaniel', 'Irish Wolfhound', 'Icelandic Sheepdog',
  'Istrian short-haired Hound', 'Istrian coarse-haired Hound',
  'Jack Russell Terrier', 'German Hunting Terrier', 'Swedish Elkhound',
  'Japanese Chin, Japanese Spaniel', 'Kai', 'Kangal Dog', 'Karakatschan',
  'Karelian bear dog', 'Karelo-Finnish Laika', 'Karst Shepherd',
  'Kerry Blue Terrier', 'Kintamani', 'King Charles Spaniel', 'Kishu',
  'Komondor', 'Kooikerhondje', 'Korean Jindo', 'Kretahound', 'Kromfohrlander',
  'Kuvasz', 'Labradoodle', 'Labrador Retriever', 'Lagotto Romagnolo',
  'Lakeland Terrier', 'Lancashire Heeler', 'Landseer',
  'Lapinporokoira, Lapponian Herder', 'Leonberger', 'Lhasa Apso',
  'Lucas Terrier', 'Norwegian Lundehund', 'Lurcher', 'Maltese',
  'Hungarian Greyhound', 'Hungarian Vizsla', 'Manchester Terrier',
  'Dutch Tulip Dog', 'Mastiff', 'Pyrenean Mastiff', 'Spanish Mastiff',
  'Middleasian greyhound - Tazi', 'Miniature Australian Shepherd',
  'Miniature Pinscher', 'Moscow Watchdog', 'Munsterlander', 'Mudi',
  'Neapolitan Mastiff', 'Newfoundland', 'Japanese Spitz', 'Nordic Spitz',
  'Norwegian Buhund', 'Norwegian Elkhound', 'Norfolk Terrier',
  'Norwich Terrier', 'Nova Scotia Duck-Tolling Retriever', 'Polish Hound',
  'Olde English Bulldogge', 'Old English Sheepdog', 'Austrian Pinscher',
  'Otterhound', 'Parson Russell Terrier', 'Garafiano Shepherd',
  'Patterdale terrier', 'Italian Greyhound', 'Pekingese', 'Spanish Water Dog',
  'Cimarron', 'Basque Shepherd Dog', 'Peruvian Hairless Dog',
  'Petit Basset Griffon Vendeen', 'Small Blue Gascony Hound', 'Löwchen',
  'Pharaoh Hound', 'Plott Hound', 'Plummer Terrier', 'Andalusian Hound',
  'Canarian Warren Hound', 'Ibizan Hound', 'Portuguese Podengo', 'Pointer',
  'Poitevin', 'Polish Lowland Sheepdog', 'Tatra Shepherd Dog', 'Porcelaine',
  'Portuguese Water Dog', 'Portuguese Pointer', 'Prague Ratter', 'Pudelpointer',
  'Pug', 'Puli', 'Pumi', 'Rampur Hound', 'Rat Terrier', 'Alentejo Mastiff',
  'Rajapalayam Hound', 'Andalusian Mouse-Hunting Dog', 'Redbone Coonhound',
  'Rhodesian Ridgeback', 'Rottweiler', 'Rough Collie', 'Russian Spaniel',
  'Russo-European Laika', 'Russian Toy Terrier', 'Saarlooswolfhond', 'Samoyed',
  'Saluki', 'Sarplaninac', 'Dutch Schapendoes', 'Schiller Hound', 'Schipperke',
  'Sealyham Terrier', 'Shetland Sheepdog', 'Scottish Terrier', 'Schnauzer',
  'Italian Hound', 'Shar Pei', 'Shiba Inu', 'Shih-Poo', 'Shih Tzu', 'Shikoku',
  'Shiloh Shepherd', 'Siberian Husky', 'Silken Windhound', 'Skye Terrier',
  'Slovakian Hound', 'Slovakian Wire-haired Pointing Dog', 'Slovakian Chuvach',
  'Spinone Italiano', 'Sloughi', 'Smalands Hound', 'Smooth Collie',
  'Serbian Hound', 'Serbian Tricolour Hound', 'Stabyhoun',
  'Staffordshire Bull Terrier', 'St. Bernard dog',
  'Styrian coarse-haired Hound', 'Finnish Hound', 'Finnish Lapphund',
  'Finnish Spitz', 'Sussex Spaniel', 'Swedish Lapphund', 'Small Swiss Hound',
  'Swiss Hound', 'Taigan', 'Taiwan Dog', 'Tamaskan Dog', 'Brasilian Terrier',
  'Tesem', 'Tibetan Spaniel', 'Bosnian -Herzegovinian -Croatian Shepherd Dog',
  'Tosa Inu, Tosa', 'Toy Manchester Terrier', 'Treeing Walker Coonhound',
  'Thai Ridgeback', 'Tibetan Terrier', 'Tyrolean Hound', 'Chinese Crested Dog',
  'Swedish Vallhund', 'Volpino Italiano', 'East Siberian Laika', 'Waeller',
  'Weimaraner', 'Welsh Corgi (Cardigan)', 'Welsh Corgi (Pembroke)',
  'Welsh Springer Spaniel', 'Welsh Terrier', 'Westfal Terrier',
  'Westphalian Dachsbracke', 'Wetterhond', 'Whippet', 'White English Bulldog',
  'Xoloitzquintle, Perro Chino Peruano', 'Yakutian Laika', 'Yorkshire terrier',
  'West Siberian Laika'
]

dogs.each do |dog_name|
  dog.variants.create!(name: dog_name, code: dog_name.parameterize)
  print "\rAdding Dog Variants: #{counter += 1} / #{dogs.count}"
end

puts ''
