puts 'Adding Pets'

Petography::Pet.find_each(&:destroy)

user = Petography::User.first

user.pets.create!(
  name:        'Fırfır',
  photo:       File.open("#{Rails.root}/db/seeds/petography/pet/photos/fırfır.jpg"),
  age:         5,
  variant:     Petography::PetVariant.find_by(code: :tekir),
  chip_number: "CHP#{rand(100000)}",
  id_number:   "IDN#{rand(100000)}",
  weight:      5.2
)

user.pets.create!(
  name:        'Kabak',
  photo:       File.open("#{Rails.root}/db/seeds/petography/pet/photos/kabak.jpg"),
  age:         2,
  variant:     Petography::PetVariant.find_by(code: :tekir),
  chip_number: "CHP#{rand(100000)}",
  id_number:   "IDN#{rand(100000)}",
  weight:      2.6
)

user.pets.create!(
  name:        'Penelope',
  photo:       File.open("#{Rails.root}/db/seeds/petography/pet/photos/penelope.jpg"),
  age:         1,
  variant:     Petography::PetVariant.find_by(code: :tekir),
  chip_number: "CHP#{rand(100000)}",
  id_number:   "IDN#{rand(100000)}",
  weight:      2.4
)
