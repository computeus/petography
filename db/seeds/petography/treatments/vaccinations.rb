Petography::Vaccination.find_each(&:destroy)

Petography::Pet.find_each do |p|
  location = Petography::Veterinary.all.sample
  p.vaccinations.create!(
    applied_at:  Time.now - rand(100).days,
    description: FFaker::Lorem.paragraph,
    location:    location,
    treatment_medicines_attributes:
                 [
                   {
                     medicine:    Petography::Medicine.all.sample,
                     amount:      rand(10),
                     amount_type: Petography::TreatmentMedicine::AMOUNT_TYPES.sample
                   },
                   {
                     medicine:    Petography::Medicine.all.sample,
                     amount:      rand(10),
                     amount_type: Petography::TreatmentMedicine::AMOUNT_TYPES.sample
                   }
                 ],
    treatment_event_attributes:
                 {
                   schedule_attributes:
                     {
                       rule:     :daily,
                       date:     (Date.today + (rand(365) + 1).days),
                       interval: (rand(5) + 1),
                       until:    (Date.today + 1.year + rand(365).days),
                       time:     Time.zone.now.beginning_of_day
                     }
                 }
  )

  location = Petography::Veterinary.all.sample
  p.vaccinations.create!(
    applied_at:  Time.now - rand(100).days,
    description: FFaker::Lorem.paragraph,
    location:    location,
    treatment_medicines_attributes:
                 [
                   {
                     medicine:    Petography::Medicine.all.sample,
                     amount:      rand(10),
                     amount_type: Petography::TreatmentMedicine::AMOUNT_TYPES.sample
                   },
                   {
                     medicine:    Petography::Medicine.all.sample,
                     amount:      rand(10),
                     amount_type: Petography::TreatmentMedicine::AMOUNT_TYPES.sample
                   }
                 ],
    treatment_event_attributes:
                 {
                   schedule_attributes:
                     {
                       rule:     :daily,
                       date:     (Date.today + (rand(365) + 1).days),
                       interval: (rand(5) + 1),
                       until:    (Date.today + 1.year + rand(365).days),
                       time:     Time.zone.now.beginning_of_day
                     }
                 }
  )
end
