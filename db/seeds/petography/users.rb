puts 'Adding Users'

Petography::User.find_each(&:destroy)

Petography::User.create!(
  email: 'user@petography.com',
  password: '123123123',
  password_confirmation: '123123123',
  fullname: 'Petography User',
  roles: [Petography::Role.find_by(code: :user)]
)

Petography::User.create!(
  email: 'pro_user@petography.com',
  password: '123123123',
  password_confirmation: '123123123',
  fullname: 'Petography Pro User',
  roles: [Petography::Role.find_by(code: :pro_user)]
)
