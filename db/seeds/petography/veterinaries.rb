puts 'Adding Veterinaries'

Petography::Veterinary.find_each(&:destroy)

Petography::Veterinary.create!(
  name:        'Petibör Veteriner Kliniği',
  description: 'Petibör Veteriner Kliniği',
  latitude:    40.912977,
  longitude:   29.227590,
  district:    Petography::District.find_by(name: 'Kartal'),
  address:     'Vatansever Cd. 38, 34876'
)
Petography::Veterinary.create!(
  name:        'Kartal Belediyesi Veteriner İşleri Müdürlüğü',
  description: 'Kartal Belediyesi Veteriner İşleri Müdürlüğü',
  latitude:    40.914941,
  longitude:   29.221814,
  district:    Petography::District.find_by(name: 'Kartal'),
  address:     'Cumhuriyet, 34876'
)
Petography::Veterinary.create!(
  name:        'Doğa Veteriner Hekimlik Hizmetleri',
  description: 'Acil Durum Veterinerlik Hizmetleri',
  latitude:    40.922291,
  longitude:   29.223326,
  district:    Petography::District.find_by(name: 'Kartal'),
  address:     'Yakacık Yeni Mh. Samandıra cad. no:32/a, 34882'
)
