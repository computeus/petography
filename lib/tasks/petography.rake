namespace :petography do
  desc 'Annotate files.'
  task annotate: :environment do
    sh 'bundle exec annotate'
  end

  desc 'Check best practices'
  task rails_best_practices: :environment do
    sh 'bundle exec rails_best_practices -f html' do; end
  end

  desc 'Alias for rails_best_practices task'
  task rbp: :rails_best_practices

  desc 'Run reek analyser'
  task reek: :environment do
    sh 'bundle exec reek --format html > reek.html' do; end
  end

  desc 'Run brakeman analyser'
  task brakeman: :environment do
    sh 'bundle exec brakeman -o brakeman.html'
  end

  desc 'Run code checked tools.'
  task check_code: :environment do
    Rake::Task['petography:annotate'].invoke
    Rake::Task['petography:reek'].invoke
    Rake::Task['petography:brakeman'].invoke
    Rake::Task['petography:rails_best_practices'].invoke
  end

  desc 'Reset counter cache columns.'
  task reset_counter_columns: :environment do
    Petography::Pet.find_each do |pet|
      Petography::Pet.reset_counters(pet.id, :vaccinations)
      Petography::Pet.reset_counters(pet.id, :cures)
      Petography::Pet.reset_counters(pet.id, :treatments)
      Petography::Pet.reset_counters(pet.id, :former_pet_diseases)
      Petography::Pet.reset_counters(pet.id, :former_pet_medicines)
    end

    Petography::Cure.find_each do |treatment|
      Petography::Cure.reset_counters(treatment.id, :reports)
    end
  end

  # Annotation fix
  Rake::Task['db:migrate'].enhance do
    Rake::Task['petography:annotate'].invoke if proper_environment?
  end

  Rake::Task['db:setup'].enhance do
    Rake::Task['petography:annotate'].invoke if proper_environment?
  end

  Rake::Task['db:rollback'].enhance do
    Rake::Task['petography:annotate'].invoke if proper_environment?
  end

  def proper_environment?
    ENV['RAILS_ENV'] == 'development' || ENV['RAILS_ENV'].blank?
  end
end
