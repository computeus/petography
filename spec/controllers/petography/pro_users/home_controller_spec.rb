require 'rails_helper'

RSpec.describe Petography::ProUsers::HomeController, type: :controller do
  before do
    sign_in(FactoryGirl.create(:petography_pro_user))
  end

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end
end
