require 'rails_helper'

RSpec.describe Petography::ProUsers::Pets::SearchesController, type: :controller do
  before do
    sign_in(FactoryGirl.create(:petography_pro_user))
  end

  describe "GET #index" do
    it "returns http success" do
      FactoryGirl.create(:petography_pet_type)

      get :index
      expect(response).to have_http_status(:success)
    end
  end
end
