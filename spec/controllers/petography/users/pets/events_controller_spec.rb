require 'rails_helper'

RSpec.describe Petography::Users::Pets::EventsController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      user = FactoryGirl.create(:petography_user)
      sign_in(user)
      pet = FactoryGirl.create(:petography_pet, user: user)
      get :index, pet_id: pet.id, format: 'json'
      expect(response).to have_http_status(:success)
    end
  end
end
