# == Schema Information
#
# Table name: petography_cities
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :petography_city, class: 'Petography::City' do
    name "MyString"
  end
end
