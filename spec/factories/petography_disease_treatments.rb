# == Schema Information
#
# Table name: petography_disease_treatments
#
#  id           :integer          not null, primary key
#  disease_id   :integer
#  treatment_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :petography_disease_treatment, class: 'Petography::DiseaseTreatment' do
    disease_id 1
    treatment_id 1
  end
end
