# == Schema Information
#
# Table name: petography_districts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  city_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :petography_district, class: 'Petography::District' do
    name "MyString"
    association :city, factory: :petography_city
  end
end
