# == Schema Information
#
# Table name: petography_former_pet_diseases
#
#  id         :integer          not null, primary key
#  pet_id     :integer
#  disease_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :petography_former_pet_disease, class: 'Petography::FormerPetDisease' do
    pet_id 1
    disease_id 1
  end
end
