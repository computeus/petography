# == Schema Information
#
# Table name: petography_former_pet_medicines
#
#  id          :integer          not null, primary key
#  pet_id      :integer
#  medicine_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :petography_former_pet_medicine, class: 'Petography::FormerPetMedicine' do
    pet_id 1
    medicine_id 1
  end
end
