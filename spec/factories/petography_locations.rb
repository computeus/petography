# == Schema Information
#
# Table name: petography_locations
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text(65535)
#  latitude    :decimal(9, 6)    default(0.0)
#  longitude   :decimal(9, 6)    default(0.0)
#  type        :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  district_id :integer
#  address     :string(255)
#  photo       :string(255)
#

FactoryGirl.define do
  factory :petography_location, class: 'Petography::Location' do
    name "MyString"
    description "MyText"
    latitude "9.99"
    longitude "9.99"
    type ""
    association :district, factory: :petography_district
    address 'Kartal İstanbul'
  end
end
