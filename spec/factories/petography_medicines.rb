# == Schema Information
#
# Table name: petography_medicines
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text(65535)
#  photo       :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :petography_medicine, class: 'Petography::Medicine' do
    name "MyString"
    description "MyTextMyText"
    photo "MyString"
  end
end
