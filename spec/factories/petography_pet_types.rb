# == Schema Information
#
# Table name: petography_pet_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :petography_pet_type, class: 'Petography::PetType' do
    name "MyString"
    code 'my-string'
  end
end
