# == Schema Information
#
# Table name: petography_pet_variants
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  code        :string(255)
#  pet_type_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :petography_pet_variant, class: 'Petography::PetVariant' do
    name "MyString"
    code 'my-string'
    association :pet_type, factory: :petography_pet_type
  end
end
