# == Schema Information
#
# Table name: petography_pet_weights
#
#  id         :integer          not null, primary key
#  pet_id     :integer
#  logged_at  :datetime
#  weight     :decimal(6, 2)    default(0.0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :petography_pet_weight, class: 'Petography::PetWeight' do
    association :pet, factory: :petography_pet
    logged_at "2016-08-14 16:46:23"
    weight 3
  end
end
