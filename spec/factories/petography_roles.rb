# == Schema Information
#
# Table name: petography_roles
#
#  id         :integer          not null, primary key
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :petography_role, class: 'Petography::Role' do
    code :user
  end

  factory :petography_role_pro_user, class: 'Petography::Role' do
    code :pro_user
  end
end
