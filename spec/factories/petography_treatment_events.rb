# == Schema Information
#
# Table name: petography_treatment_events
#
#  id           :integer          not null, primary key
#  treatment_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :petography_treatment_event, class: 'Petography::TreatmentEvent' do
    treatment_id 1
  end
end
