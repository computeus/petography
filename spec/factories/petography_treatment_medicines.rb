# == Schema Information
#
# Table name: petography_treatment_medicines
#
#  id           :integer          not null, primary key
#  treatment_id :integer
#  medicine_id  :integer
#  amount       :integer
#  amount_type  :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :petography_treatment_medicine, class: 'Petography::TreatmentMedicine' do
    treatment_id 1
    medicine_id 1
    amount 1
    amount_type "MyString"
  end
end
