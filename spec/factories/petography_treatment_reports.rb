# == Schema Information
#
# Table name: petography_treatment_reports
#
#  id           :integer          not null, primary key
#  report_date  :datetime
#  file         :string(255)
#  treatment_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :petography_treatment_report, class: 'Petography::TreatmentReport' do
    report_date "2016-07-29 18:39:10"
    file "MyString"
  end
end
