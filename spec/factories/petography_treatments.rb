# == Schema Information
#
# Table name: petography_treatments
#
#  id                      :integer          not null, primary key
#  applied_at              :datetime
#  description             :text(65535)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  pet_id                  :integer
#  location_id             :integer
#  type                    :string(255)
#  treatment_reports_count :integer          default(0)
#

FactoryGirl.define do
  factory :petography_treatment, class: 'Petography::Treatment' do
    applied_at "2016-07-08 13:37:11"
    description "MyString"
  end
end
