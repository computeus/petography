# == Schema Information
#
# Table name: petography_user_roles
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  role_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :petography_user_role, class: 'Petography::UserRole' do
    association :user, factory: :petography_user
    association :role, factory: :petography_role

    factory :petography_user_role_pro_user do
      association :role, factory: :petography_role_pro_user
    end
  end
end
