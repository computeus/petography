FactoryGirl.define do
  factory :petography_vaccination, class: 'Petography::Vaccination' do
    association :pet, factory: :petography_pet
    association :location, factory: :petography_location
    description 'Vele vele vele'
  end
end
