# == Schema Information
#
# Table name: petography_locations
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text(65535)
#  latitude    :decimal(9, 6)    default(0.0)
#  longitude   :decimal(9, 6)    default(0.0)
#  type        :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  district_id :integer
#  address     :string(255)
#  photo       :string(255)
#

require 'rails_helper'

RSpec.describe Petography::Location, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
