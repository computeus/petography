# == Schema Information
#
# Table name: petography_pets
#
#  id                         :integer          not null, primary key
#  name                       :string(255)
#  photo                      :string(255)
#  birthdate                  :date
#  pet_variant_id             :integer
#  user_id                    :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  chip_number                :string(255)
#  id_number                  :string(255)
#  cures_count                :integer          default(0)
#  vaccinations_count         :integer          default(0)
#  treatments_count           :integer          default(0)
#  former_pet_diseases_count  :integer          default(0)
#  former_pet_medicines_count :integer          default(0)
#

require 'rails_helper'

RSpec.describe Petography::Pet, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
