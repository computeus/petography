require 'rails_helper'

RSpec.describe "Petography::Users::PetTypes::Variants", type: :request do
  before do
    @user = FactoryGirl.create(:petography_user, password: 'password')
    sign_in_as_a_valid_user
  end

  describe "GET /petography_users_pet_types_get_pet_variants" do
    let(:pet_type) { FactoryGirl.create(:petography_pet_type) }

    it "works! (now write some real specs)" do
      get users_pet_type_variants_path(pet_type, format: :json)
      expect(response).to have_http_status(200)
    end
  end
end
