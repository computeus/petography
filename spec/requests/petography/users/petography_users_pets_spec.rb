require 'rails_helper'

RSpec.describe "Petography::Users::Pets", type: :request do
  before do
    @user = FactoryGirl.create(:petography_user, password: 'password')
    sign_in_as_a_valid_user
  end

  describe "GET /petography_users_pets" do
    it "works! (now write some real specs)" do
      get users_pets_path
      expect(response).to have_http_status(200)
    end
  end
end
