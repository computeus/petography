require 'rails_helper'

RSpec.describe "Petography::Users::Vaccinations", type: :request do
  before do
    @user = FactoryGirl.create(:petography_user, password: 'password')
    sign_in_as_a_valid_user
  end

  describe "GET /petography_users_vaccinations" do
    let(:pet) { FactoryGirl.create(:petography_pet, user: @user) }

    it "works! (now write some real specs)" do
      get new_users_pet_vaccination_path(pet)
      expect(response).to have_http_status(200)
    end
  end
end
