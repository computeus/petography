def login(email, password)
  post_via_redirect user_session_path, user: { email: email, password: password }
end
