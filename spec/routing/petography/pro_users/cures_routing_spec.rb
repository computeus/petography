require "rails_helper"

RSpec.describe Petography::ProUsers::CuresController, type: :routing do
  describe "routing" do

    it "routes to #new" do
      expect(:get => "/pro_users/pets/1/cures/new").to route_to("petography/pro_users/cures#new", pet_id: "1")
    end

    it "routes to #show" do
      expect(:get => "/pro_users/pets/1/cures/1").to route_to("petography/pro_users/cures#show", id: "1", pet_id: "1")
    end

    it "routes to #edit" do
      expect(:get => "/pro_users/pets/1/cures/1/edit").to route_to("petography/pro_users/cures#edit", id: "1", pet_id: "1")
    end

    it "routes to #create" do
      expect(:post => "/pro_users/pets/1/cures").to route_to("petography/pro_users/cures#create", pet_id: "1")
    end

    it "routes to #update via PUT" do
      expect(:put => "/pro_users/pets/1/cures/1").to route_to("petography/pro_users/cures#update", id: "1", pet_id: "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/pro_users/pets/1/cures/1").to route_to("petography/pro_users/cures#update", id: "1", pet_id: "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/pro_users/pets/1/cures/1").to route_to("petography/pro_users/cures#destroy", id: "1", pet_id: "1")
    end

  end
end
