require "rails_helper"

RSpec.describe Petography::ProUsers::PetTypes::VariantsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/pro_users/pet_types/1/variants").to route_to("petography/pro_users/pet_types/variants#index", pet_type_id: "1")
    end
  end
end
