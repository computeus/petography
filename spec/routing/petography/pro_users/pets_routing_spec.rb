require "rails_helper"

RSpec.describe Petography::ProUsers::PetsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/pro_users/pets").to route_to("petography/pro_users/pets#index")
    end

    it "routes to #new" do
      expect(:get => "/pro_users/pets/new").to route_to("petography/pro_users/pets#new")
    end

    it "routes to #show" do
      expect(:get => "/pro_users/pets/1").to route_to("petography/pro_users/pets#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/pro_users/pets/1/edit").to route_to("petography/pro_users/pets#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/pro_users/pets").to route_to("petography/pro_users/pets#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/pro_users/pets/1").to route_to("petography/pro_users/pets#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/pro_users/pets/1").to route_to("petography/pro_users/pets#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/pro_users/pets/1").to route_to("petography/pro_users/pets#destroy", :id => "1")
    end
  end
end


