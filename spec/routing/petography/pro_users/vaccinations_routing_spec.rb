require "rails_helper"

RSpec.describe Petography::ProUsers::VaccinationsController, type: :routing do
  describe "routing" do

    it "routes to #new" do
      expect(:get => "/pro_users/pets/1/vaccinations/new").to route_to("petography/pro_users/vaccinations#new", pet_id: "1")
    end

    it "routes to #show" do
      expect(:get => "/pro_users/pets/1/vaccinations/1").to route_to("petography/pro_users/vaccinations#show", :id => "1", pet_id: "1")
    end

    it "routes to #edit" do
      expect(:get => "/pro_users/pets/1/vaccinations/1/edit").to route_to("petography/pro_users/vaccinations#edit", :id => "1", pet_id: "1")
    end

    it "routes to #create" do
      expect(:post => "/pro_users/pets/1/vaccinations").to route_to("petography/pro_users/vaccinations#create", pet_id: "1")
    end

    it "routes to #update via PUT" do
      expect(:put => "/pro_users/pets/1/vaccinations/1").to route_to("petography/pro_users/vaccinations#update", :id => "1", pet_id: "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/pro_users/pets/1/vaccinations/1").to route_to("petography/pro_users/vaccinations#update", :id => "1", pet_id: "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/pro_users/pets/1/vaccinations/1").to route_to("petography/pro_users/vaccinations#destroy", :id => "1", pet_id: "1")
    end

  end
end
