require "rails_helper"

RSpec.describe Petography::ProUsers::WeightsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/pro_users/pets/1/weights").to route_to("petography/pro_users/weights#index", pet_id: "1")
    end

    it "routes to #new" do
      expect(:get => "/pro_users/pets/1/weights/new").to route_to("petography/pro_users/weights#new", pet_id: "1")
    end

    it "routes to #edit" do
      expect(:get => "/pro_users/pets/1/weights/1/edit").to route_to("petography/pro_users/weights#edit", id: "1", pet_id: "1")
    end

    it "routes to #create" do
      expect(:post => "/pro_users/pets/1/weights").to route_to("petography/pro_users/weights#create", pet_id: "1")
    end

    it "routes to #update via PUT" do
      expect(:put => "/pro_users/pets/1/weights/1").to route_to("petography/pro_users/weights#update", id: "1", pet_id: "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/pro_users/pets/1/weights/1").to route_to("petography/pro_users/weights#update", id: "1", pet_id: "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/pro_users/pets/1/weights/1").to route_to("petography/pro_users/weights#destroy", id: "1", pet_id: "1")
    end
  end
end
