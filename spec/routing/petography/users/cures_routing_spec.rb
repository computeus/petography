require "rails_helper"

RSpec.describe Petography::Users::CuresController, type: :routing do
  describe "routing" do

    it "routes to #new" do
      expect(:get => "/users/pets/1/cures/new").to route_to("petography/users/cures#new", pet_id: "1")
    end

    it "routes to #show" do
      expect(:get => "/users/pets/1/cures/1").to route_to("petography/users/cures#show", id: "1", pet_id: "1")
    end

    it "routes to #edit" do
      expect(:get => "/users/pets/1/cures/1/edit").to route_to("petography/users/cures#edit", id: "1", pet_id: "1")
    end

    it "routes to #create" do
      expect(:post => "/users/pets/1/cures").to route_to("petography/users/cures#create", pet_id: "1")
    end

    it "routes to #update via PUT" do
      expect(:put => "/users/pets/1/cures/1").to route_to("petography/users/cures#update", id: "1", pet_id: "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/users/pets/1/cures/1").to route_to("petography/users/cures#update", id: "1", pet_id: "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/users/pets/1/cures/1").to route_to("petography/users/cures#destroy", id: "1", pet_id: "1")
    end

  end
end
