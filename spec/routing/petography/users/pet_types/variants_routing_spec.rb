require "rails_helper"

RSpec.describe Petography::Users::PetTypes::VariantsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/users/pet_types/1/variants").to route_to("petography/users/pet_types/variants#index", pet_type_id: "1")
    end
  end
end
