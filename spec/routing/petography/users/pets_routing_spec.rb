require "rails_helper"

RSpec.describe Petography::Users::PetsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/users/pets").to route_to("petography/users/pets#index")
    end

    it "routes to #new" do
      expect(:get => "/users/pets/new").to route_to("petography/users/pets#new")
    end

    it "routes to #show" do
      expect(:get => "/users/pets/1").to route_to("petography/users/pets#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/users/pets/1/edit").to route_to("petography/users/pets#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/users/pets").to route_to("petography/users/pets#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/users/pets/1").to route_to("petography/users/pets#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/users/pets/1").to route_to("petography/users/pets#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/users/pets/1").to route_to("petography/users/pets#destroy", :id => "1")
    end
  end
end


