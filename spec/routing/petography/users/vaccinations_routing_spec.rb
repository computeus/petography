require "rails_helper"

RSpec.describe Petography::Users::VaccinationsController, type: :routing do
  describe "routing" do

    it "routes to #new" do
      expect(:get => "/users/pets/1/vaccinations/new").to route_to("petography/users/vaccinations#new", pet_id: "1")
    end

    it "routes to #show" do
      expect(:get => "/users/pets/1/vaccinations/1").to route_to("petography/users/vaccinations#show", :id => "1", pet_id: "1")
    end

    it "routes to #edit" do
      expect(:get => "/users/pets/1/vaccinations/1/edit").to route_to("petography/users/vaccinations#edit", :id => "1", pet_id: "1")
    end

    it "routes to #create" do
      expect(:post => "/users/pets/1/vaccinations").to route_to("petography/users/vaccinations#create", pet_id: "1")
    end

    it "routes to #update via PUT" do
      expect(:put => "/users/pets/1/vaccinations/1").to route_to("petography/users/vaccinations#update", :id => "1", pet_id: "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/users/pets/1/vaccinations/1").to route_to("petography/users/vaccinations#update", :id => "1", pet_id: "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/users/pets/1/vaccinations/1").to route_to("petography/users/vaccinations#destroy", :id => "1", pet_id: "1")
    end

  end
end
