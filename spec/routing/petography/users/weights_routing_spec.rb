require "rails_helper"

RSpec.describe Petography::Users::WeightsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/users/pets/1/weights").to route_to("petography/users/weights#index", pet_id: "1")
    end

    it "routes to #new" do
      expect(:get => "/users/pets/1/weights/new").to route_to("petography/users/weights#new", pet_id: "1")
    end

    it "routes to #edit" do
      expect(:get => "/users/pets/1/weights/1/edit").to route_to("petography/users/weights#edit", id: "1", pet_id: "1")
    end

    it "routes to #create" do
      expect(:post => "/users/pets/1/weights").to route_to("petography/users/weights#create", pet_id: "1")
    end

    it "routes to #update via PUT" do
      expect(:put => "/users/pets/1/weights/1").to route_to("petography/users/weights#update", id: "1", pet_id: "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/users/pets/1/weights/1").to route_to("petography/users/weights#update", id: "1", pet_id: "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/users/pets/1/weights/1").to route_to("petography/users/weights#destroy", id: "1", pet_id: "1")
    end
  end
end
