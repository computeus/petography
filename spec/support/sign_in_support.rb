# #module for helping controller specs
# module ValidUserHelper
#   def signed_in_as_a_valid_user
#     @user ||= FactoryGirl.create :user
#     sign_in @user # method from devise:TestHelpers
#   end
# end

# module for helping request specs
module ValidUserRequestHelper
  include Warden::Test::Helpers

  # for use in request specs
  def sign_in_as_a_valid_user
    @user ||= FactoryGirl.create :petography_user
    # Use this
    # post_via_redirect user_session_path, user: { email: @user.email, password: @user.password }

    # Or These
    scope = Devise::Mapping.find_scope!(@user)
    login_as(@user, scope: scope)
  end
end
