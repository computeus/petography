require 'rails_helper'

RSpec.describe "petography/pro_users/vaccinations/new", type: :view do
  before(:each) do
    vaccination = assign(:vaccination, FactoryGirl.create(:petography_vaccination))
    @pet = assign(:pet, vaccination.pet)
    assign(:locations, Petography::Location.all)
    assign(:medicines, [])
    assign(:extra_field_local_variables, {})
  end

  it "renders new petography_vaccination form" do
    render

    assert_select "form[action=?][method=?]", pro_users_pet_vaccinations_path(@pet), "post" do
    end
  end
end
