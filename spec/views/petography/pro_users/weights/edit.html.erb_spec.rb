require 'rails_helper'

RSpec.describe "petography/pro_users/weights/edit", type: :view do
  before(:each) do
    @pet = assign(:pet, FactoryGirl.create(:petography_pet))
    @weight = assign(:weight, FactoryGirl.create(:petography_pet_weight, pet: @pet))
  end

  it "renders the edit petography_user form" do
    render

    assert_select "form[action=?][method=?]", pro_users_pet_weight_path(@pet, @weight), "post" do

      assert_select "input#petography_pet_weight_weight[name=?]", "petography_pet_weight[weight]"
    end
  end
end
