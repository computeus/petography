require 'rails_helper'

RSpec.describe "petography/pro_users/weights/index", type: :view do
  before(:each) do
    pet = assign(:pet, FactoryGirl.create(:petography_pet))
    assign(:weights, [
      FactoryGirl.create(:petography_pet_weight, pet: pet),
      FactoryGirl.create(:petography_pet_weight, pet: pet)
    ])
  end

  it "renders a list of pet weights" do
    render
    assert_select "tr>td", text: "3.0".to_s, count: 2
  end
end
