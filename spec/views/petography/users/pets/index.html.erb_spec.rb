require 'rails_helper'

RSpec.describe "petography/users/pets/index", type: :view do
  before(:each) do
    user = FactoryGirl.create(:petography_user)
    assign(:pets, Kaminari.paginate_array([
      FactoryGirl.create(:petography_pet, user: user, name: 'Fırfır'),
      FactoryGirl.create(:petography_pet, user: user, name: 'Fırfır')
    ]).page(1))
  end

  it "renders a list of petography/users" do
    render
    assert_select "tr>td", :text => "Fırfır".to_s, :count => 2
  end
end

