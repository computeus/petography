require 'rails_helper'

RSpec.describe "petography/users/pets/new", type: :view do
  before(:each) do
    pet = assign(:pet, FactoryGirl.create(:petography_pet))
    assign(:pet_type, pet.pet_type)
    assign(:pet_types, [pet.pet_type])
    assign(:pet_variants, pet.pet_type.variants)
    assign(:diseases, [FactoryGirl.create(:petography_disease)])
    assign(:medicines, [FactoryGirl.create(:petography_medicine)])
  end

  it "renders new petography_user form" do
    render

    assert_select "form[action=?][method=?]", users_pets_path, "post" do

      assert_select "input#petography_pet_name[name=?]", "petography_pet[name]"

      assert_select "input#petography_pet_birthdate[name=?]", "petography_pet[birthdate]"

      assert_select "input#petography_pet_age[name=?]", "petography_pet[age]"

      assert_select "input#petography_pet_id_number[name=?]", "petography_pet[id_number]"
    end
  end
end
