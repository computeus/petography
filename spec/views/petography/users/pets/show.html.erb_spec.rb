require 'rails_helper'

RSpec.describe "petography/users/pets/show", type: :view do
  before(:each) do
    user = assign(:user, FactoryGirl.create(:petography_user))
    @pet = assign(:pet, FactoryGirl.create(:petography_pet, user: user))
    assign(:activities, Kaminari.paginate_array([]).page(1))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/#{@pet.name}/)
    expect(rendered).to match(/#{@pet.photo.url(:middle)}/)
    expect(rendered).to match(/#{@pet.variant_label}/)
    expect(rendered).to match(/#{@pet.weight}/)
  end
end
