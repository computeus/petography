require 'rails_helper'

RSpec.describe "petography/users/vaccinations/edit", type: :view do
  before(:each) do
    @pet = assign(:pet, FactoryGirl.create(:petography_pet))
    @vaccination = assign(:vaccination, FactoryGirl.create(:petography_vaccination, pet: @pet))
    assign(:locations, [])
    assign(:medicines, [])
    assign(:extra_field_local_variables, { })
  end

  it "renders the edit petography_user form" do
    render

    assert_select "form[action=?][method=?]", users_pet_vaccination_path(@pet, @vaccination), "post" do
    end
  end
end
