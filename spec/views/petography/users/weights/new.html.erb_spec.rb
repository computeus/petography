require 'rails_helper'

RSpec.describe "petography/users/weights/new", type: :view do
  before(:each) do
    @pet = assign(:pet, FactoryGirl.create(:petography_pet))
    assign(:weight, FactoryGirl.create(:petography_pet_weight, pet: @pet))
  end

  it "renders new petography_user form" do
    render

    assert_select "form[action=?][method=?]", users_pet_weights_path(@pet), "post" do

      assert_select "input#petography_pet_weight_weight[name=?]", "petography_pet_weight[weight]"
    end
  end
end
